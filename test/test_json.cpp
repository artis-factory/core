/**
 * @file test_json.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>

#include <chrono>
#include <iostream>

#define BOOST_TEST_MODULE Factory_JSON_Tests

#include <boost/test/unit_test.hpp>

#include "FactoryGraphManager.hpp"
#include "json_dsl/model/JsonReader.hpp"
#include "dsl_to_devs/Transformer.hpp"

using namespace std::chrono;

class JsonPoolRouterView : public artis::factory::View {
public:
  JsonPoolRouterView(unsigned int pool_number) {
//    for (int i = 0; i < (int) zone_number; ++i) {
//      selector("PoolRouter_" + std::to_string(i) + ":waiting_po_number",
//               {artis::factory::FactoryGraphManager::POOL_ROUTER + i,
//                artis::factory::PoolRouter::vars::WAITING_PO_NUMBER});
//    }
//    selector("Generator:total_po_number",
//             {artis::factory::FactoryGraphManager::GENERATOR,
//              artis::factory::ProductionOrderGenerator::vars::TOTAL_PO_NUMBER});
  }
};

BOOST_AUTO_TEST_CASE(TestCase_JSONPool)
{
  std::ifstream input("../../data/new_factory.json");

  if (input) {
    std::string str((std::istreambuf_iterator<char>(input)), std::istreambuf_iterator<char>());
    artis::factory::json_dsl::JsonReader reader;
    artis::factory::dsl_to_devs::Transformer transformer;

    reader.parse(str);

    artis::factory::FactoryGraphManagerParameters graph_parameters{transformer(reader.factory())};
    artis::common::context::Context<artis::common::DoubleTime> context(0, 8 * 3600); // 8h
    artis::common::RootCoordinator<
      artis::common::DoubleTime, artis::pdevs::Coordinator<
        artis::common::DoubleTime, artis::factory::FactoryGraphManager, artis::common::NoParameters, artis::factory::FactoryGraphManagerParameters>
    > rc(context, "root", artis::common::NoParameters(), graph_parameters);

    std::cout << rc.to_string() << std::endl;

//    rc.attachView("PoolRouter", new JsonPoolRouterView(reader.factory()._pools.size()));
    rc.switch_to_timed_observer(1);

    steady_clock::time_point t1 = steady_clock::now();

    rc.run(context);

    steady_clock::time_point t2 = steady_clock::now();

    duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

    std::cout << "Duration: " << time_span.count() << std::endl;

    artis::factory::Output output(rc.observer());

    output(context.begin(), context.end(), {context.begin(), 1});

    std::cout << "=== ROUTER ===" << std::endl;

    std::cout << artis::factory::Trace::trace().elements().filter_level_type(artis::common::LevelType::USER).filter_model_name("root:R").to_string()
              << std::endl;

    std::cout << "=== M111 ===" << std::endl;

    std::cout << artis::factory::Trace::trace().elements().filter_level_type(artis::common::LevelType::USER).filter_model_name("root:Z1:Z11:M_M111").to_string()
              << std::endl;

    std::cout << "=== M112 ===" << std::endl;

    std::cout << artis::factory::Trace::trace().elements().filter_level_type(artis::common::LevelType::USER).filter_model_name("root:Z1:Z11:M_M112").to_string()
              << std::endl;

    std::cout << "=== M121 ===" << std::endl;

    std::cout << artis::factory::Trace::trace().elements().filter_level_type(artis::common::LevelType::USER).filter_model_name("root:Z1:Z12:M_M121").to_string()
              << std::endl;

    std::cout << "=== M122 ===" << std::endl;

    std::cout << artis::factory::Trace::trace().elements().filter_level_type(artis::common::LevelType::USER).filter_model_name("root:Z1:Z12:M_M122").to_string()
              << std::endl;

    std::cout << "=== M123 ===" << std::endl;

    std::cout << artis::factory::Trace::trace().elements().filter_level_type(artis::common::LevelType::USER).filter_model_name("root:Z1:Z12:M_M123").to_string()
              << std::endl;

    std::cout << "=== M131 ===" << std::endl;

    std::cout << artis::factory::Trace::trace().elements().filter_level_type(artis::common::LevelType::USER).filter_model_name("root:Z1:Z13:M_M131").to_string()
              << std::endl;

    std::cout << "=== M132 ===" << std::endl;

    std::cout << artis::factory::Trace::trace().elements().filter_level_type(artis::common::LevelType::USER).filter_model_name("root:Z1:Z13:M_M132").to_string()
              << std::endl;

    std::cout << "=== M21 ===" << std::endl;

    std::cout << artis::factory::Trace::trace().elements().filter_level_type(artis::common::LevelType::USER).filter_model_name("root:Z2:M_M21").to_string()
              << std::endl;

    std::cout << "=== B_111 ===" << std::endl;

    std::cout << artis::factory::Trace::trace().elements().filter_level_type(artis::common::LevelType::USER).filter_model_name("root:Z1:Z11:B_B111").to_string()
              << std::endl;

    std::cout << "=== S_S1 ===" << std::endl;

    std::cout << artis::factory::Trace::trace().elements().filter_level_type(artis::common::LevelType::USER).filter_model_name("root:S_S1").to_string()
              << std::endl;

    std::cout << "=== S_S2 ===" << std::endl;

    std::cout << artis::factory::Trace::trace().elements().filter_level_type(artis::common::LevelType::USER).filter_model_name("root:Z1:S_S2").to_string()
              << std::endl;

    std::cout << "=== S_S3 ===" << std::endl;

    std::cout << artis::factory::Trace::trace().elements().filter_level_type(artis::common::LevelType::USER).filter_model_name("root:Z1:Z12:S_S3").to_string()
              << std::endl;

    std::cout << "=== S_S4 ===" << std::endl;

    std::cout << artis::factory::Trace::trace().elements().filter_level_type(artis::common::LevelType::USER).filter_model_name("root:Z1:S_S4").to_string()
              << std::endl;

    std::cout << "=== S_S5 ===" << std::endl;

    std::cout << artis::factory::Trace::trace().elements().filter_level_type(artis::common::LevelType::USER).filter_model_name("root:S_S5").to_string()
              << std::endl;

    std::cout << "=== S_S6 ===" << std::endl;

    std::cout << artis::factory::Trace::trace().elements().filter_level_type(artis::common::LevelType::USER).filter_model_name("root:S_S6").to_string()
              << std::endl;

    BOOST_CHECK(true);
  } else {
    BOOST_CHECK(false);
  }
}