/**
 * @file test_json_schema.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_MODULE Factory_Json_Schema_Tests

#include <fstream>
#include <iostream>

#include <boost/test/unit_test.hpp>

#include "json_dsl/validator/nlohmann/json-schema.hpp"

using nlohmann::json;
using nlohmann::json_schema::json_validator;

class custom_error_handler : public nlohmann::json_schema::basic_error_handler {
  void error(const json::json_pointer &pointer, const json &instance,
             const std::string &message) override {
    nlohmann::json_schema::basic_error_handler::error(pointer, instance, message);
    std::cerr << "ERROR: '" << pointer << "' - '" << instance << "': " << message << "\n";
  }
};

BOOST_AUTO_TEST_CASE(TestCase_OneProduct_OneZone_ThreeMachines)
{
  std::ifstream input("../../schema/factory.schema.json");

  if (input) {
    std::string str((std::istreambuf_iterator<char>(input)), std::istreambuf_iterator<char>());
    json factory_schema = nlohmann::json::parse(str);
    json_validator validator;

    try {
      validator.set_root_schema(factory_schema);
    } catch (const std::exception &e) {
      std::cerr << "Validation of json_dsl failed, here is why: " << e.what() << "\n";
      BOOST_CHECK(false);
    }

    json factory = R"(
{
  "ID": "avosdim",
  "catalog": [
    {
      "ID": "P1",
      "program": {
        "first_operation": {
          "processing_time": 10,
          "machineID": "M1",
          "zoneID": "line-1",
          "next_operation": {
            "processing_time": 10,
            "machineID": "M2",
            "zoneID": "line-1"
          }
        }
      }
    }
  ],
  "zones": [
    {
      "ID": "line-1",
      "index": 1,
      "type": {
        "ID": "main"
      },
      "machines": [
        {
          "ID": "M1",
          "index": 1,
          "type": "processor",
          "preemption": true
        },
        {
          "ID": "M2",
          "index": 2,
          "type": "processor"
        },
        {
          "ID": "M3",
          "index": 3,
          "type": "processor"
        }
      ]
    }
  ]
}
    )"_json;

    try {
      custom_error_handler err;

      validator.validate(factory, err);
    } catch (const std::exception &e) {
      std::cerr << "Validation failed" << std::endl;
      BOOST_CHECK(false);
    }
  }
  BOOST_CHECK(true);
}