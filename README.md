# artis-factory

The objective of this repo is to create a simulation of a manufacturing factory by using
the Artis* framework

Artis* repo: https://gitlab.com/artis-star/artis-star

## Local Installation

To run this project, you need Artis.

First, you have to install these packets:

```bash
apt-get install -y \
cmake \
make \
git \
pkg-config \
g++ \
libboost-dev \
libboost-mpi-dev \
libboost-timer-dev \
libboost-serialization-dev \
libboost-system-dev \
libboost-test-dev
```

Then, to install Artis*:

```bash
cd $HOME
mkdir usr
cd usr
echo "export PKG_CONFIG_PATH=\$PKG_CONFIG_PATH:\$HOME/usr/lib/pkgconfig" >> $HOME/.bashrc
source $HOME/.bashrc

git clone https://gitlab.com/artis-star/artis-star.git
cd artis-star
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$HOME/usr -DCMAKE_BUILD_TYPE=Debug ..
make install
```

### Run artis-factory

- Install the nlohmann json library (**version 3.10 required**)

```bash
sudo apt install nlohmann-json3-dev
```

To build and run the project:

```bash
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$HOME/usr -DCMAKE_BUILD_TYPE=Debug ..
make
cd test
./test_json
```