/**
 * @file ZoneGraphManager.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_FACTORY_ZONE_GRAPH_MANAGER_HPP
#define ARTIS_FACTORY_ZONE_GRAPH_MANAGER_HPP

#include "Base.hpp"
#include "Buffer.hpp"
#include "dsl_to_devs/Model.hpp"
#include "Processor.hpp"
#include "Sink.hpp"
#include "Storage.hpp"
#include "ZoneRouter.hpp"

#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>

#include <stack>

namespace artis::factory {

struct ZoneGraphManagerParameters {
  const std::string ID;
  const uint index;
  const dsl_to_devs::Zones &sub_zones;
  const dsl_to_devs::Machines &machines;
  const dsl_to_devs::ProductInOuts &in_outs;
  const dsl_to_devs::Stocks &stocks;
  const dsl_to_devs::Storages &storages;
  const dsl_to_devs::Buffers &buffers;
  const dsl_to_devs::MachineProductOperationDurations &operation_durations;
  const dsl_to_devs::StorageMachineExchanges &storage_machine_exchanges;
  const std::vector<std::pair<uint, uint>> &all_machines;
};

class ZoneGraphManager
  : public artis::pdevs::GraphManager<artis::common::DoubleTime, artis::common::NoParameters, ZoneGraphManagerParameters> {
public:
  struct inputs {
    enum values {
      IN_PO, IN_ITEM = 1000, IN_ITEM_AVAILABLE = 2000, IN_SPACE_AVAILABLE = 3000
    };
  };

  struct outputs {
    enum values {
      OUT_PO, OUT_ITEM = 1000, OUT_ITEM_AVAILABLE = 2000, OUT_SPACE_AVAILABLE = 3000
    };
  };

  struct sub_models {
    enum values {
      ZONE_ROUTER, MACHINE = 10000, SINK = 20000, BUFFER = 30000, STORAGE = 40000, SUB_ZONE = 50000
    };
  };

  ZoneGraphManager(Coordinator *coordinator, const artis::common::NoParameters &parameters,
                   const ZoneGraphManagerParameters &graph_parameters) :
    artis::pdevs::GraphManager<artis::common::DoubleTime, artis::common::NoParameters, ZoneGraphManagerParameters>(
      coordinator, parameters, graph_parameters) {

    // coordinator => OK
    {
      coordinator->input_port({inputs::IN_PO, "in_po"});
      coordinator->output_port({outputs::OUT_PO, "out_po"});
    }

    // zone router => OK
    {
      _router = std::make_unique<ZoneRouterSimulator>("R",
                                                      ZoneRouterParameters{graph_parameters.ID, graph_parameters.index,
                                                                           graph_parameters.machines,
                                                                           graph_parameters.sub_zones});
      this->add_child(sub_models::ZONE_ROUTER, _router.get());
      in({coordinator, inputs::IN_PO})
        >> in({_router.get(), artis::factory::ZoneRouter::inputs::IN_PO});
      out({_router.get(), artis::factory::ZoneRouter::outputs::OUT_PO})
        >> out({coordinator, outputs::OUT_PO});
    }

    // machines => OK
    {
      for (const auto &m: graph_parameters.machines) {
        auto machine_index = m.index;

        switch (m.type) {
          case PROCESSOR: {
            ProcessorParameters processor_parameters(machine_index, m.type, graph_parameters.index, m.capacity,
                                                     m.loading_time, m.setup_time,
                                                     m.processing_time, m.unloading_time,
                                                     graph_parameters.stocks,
                                                     graph_parameters.in_outs,
                                                     graph_parameters.operation_durations);

            _processors[machine_index] = std::make_unique<ProcessorSimulator>("M_" + m.id, processor_parameters);
            this->add_child(sub_models::MACHINE + machine_index, _processors[machine_index].get());

            // connection processor <-> zone router
            out({_router.get(), artis::factory::ZoneRouter::outputs::OUT_M + machine_index})
              >> in({_processors[machine_index].get(), artis::factory::Processor::inputs::IN_PO});
            out({_processors[machine_index].get(), artis::factory::Processor::outputs::OUT_PO})
              >> in({_router.get(), artis::factory::ZoneRouter::inputs::IN_M + machine_index});
            break;
          }
          case SINK: {
            // TODO
            break;
          }
          case SEPARATOR: {
            // TODO
            break;
          }
          case COMBINER : {
            // TODO
            break;
          }
          case CONVEYOR : {
            // TODO
            break;
          }
          default: {
          }
        }
      }
    }

    // sinks
    // TODO

    // buffers => OK
    {
      for (const auto &buffer: graph_parameters.buffers) {
        BufferParameters buffer_parameters{
          {graph_parameters.stocks.buffer_machines.find(buffer.index)->second.ins,
           graph_parameters.stocks.buffer_machines.find(buffer.index)->second.outs,
           (int) buffer.capacity}};

        _buffers.push_back(std::make_unique<BufferSimulator>("B_" + buffer.id, buffer_parameters));
        this->add_child(sub_models::BUFFER + buffer.index, _buffers.back().get());

        const auto &in_machines = graph_parameters.stocks.buffer_machines.find(buffer.index)->second.ins;
        const auto &out_machines = graph_parameters.stocks.buffer_machines.find(buffer.index)->second.outs;

        for (const auto &machine_id: out_machines) {
          out({_buffers.back().get(), artis::factory::Buffer::outputs::OUT_ITEM + machine_id})
            >> in({_processors[machine_id].get(), artis::factory::Processor::inputs::IN_ITEM + buffer.index});
          out({_processors[machine_id].get(), artis::factory::Processor::outputs::OUT_ITEM_AVAILABLE + buffer.index})
            >> in({_buffers.back().get(), artis::factory::Buffer::inputs::IN_ITEM_AVAILABLE + machine_id});
          out({_buffers.back().get(), artis::factory::Buffer::outputs::OUT_ITEM_AVAILABLE + machine_id})
            >> in({_processors[machine_id].get(), artis::factory::Processor::inputs::IN_ITEM_AVAILABLE + buffer.index});
        }
        for (const auto &machine_id: in_machines) {
          out({_processors[machine_id].get(), artis::factory::Processor::outputs::OUT_ITEM + buffer.index})
            >> in({_buffers.back().get(), artis::factory::Buffer::inputs::IN_ITEM + machine_id});
          out({_processors[machine_id].get(), artis::factory::Processor::outputs::OUT_SPACE_AVAILABLE + buffer.index})
            >> in({_buffers.back().get(), artis::factory::Buffer::inputs::IN_SPACE_AVAILABLE + machine_id});
          out({_buffers.back().get(), artis::factory::Buffer::outputs::OUT_SPACE_AVAILABLE + machine_id})
            >> in(
              {_processors[machine_id].get(), artis::factory::Processor::inputs::IN_SPACE_AVAILABLE + buffer.index});
        }
      }
    }

    // storages => OK
    {
      for (const auto &storage: graph_parameters.storages) {
        StorageParameters storage_parameters{
          {graph_parameters.storage_machine_exchanges.ins.find(storage.index)->second.second,
           graph_parameters.storage_machine_exchanges.outs.find(storage.index)->second.second,
           (int) storage.capacity}};

        _storages[storage.index] = std::make_unique<StorageSimulator>("S_" + storage.id, storage_parameters);
        this->add_child(sub_models::STORAGE + storage.index, _storages[storage.index].get());
        for (const auto &machine_id: graph_parameters.storage_machine_exchanges.outs.find(
          storage.index)->second.second) {
          if (_processors.find(machine_id) != _processors.end()) {
            out({_storages[storage.index].get(), artis::factory::Buffer::outputs::OUT_ITEM + machine_id})
              >> in({_processors[machine_id].get(), artis::factory::Processor::inputs::IN_ITEM + storage.index});
            out({_processors[machine_id].get(), artis::factory::Processor::outputs::OUT_ITEM_AVAILABLE + storage.index})
              >> in({_storages[storage.index].get(), artis::factory::Buffer::inputs::IN_ITEM_AVAILABLE + machine_id});
            out({_storages[storage.index].get(), artis::factory::Buffer::outputs::OUT_ITEM_AVAILABLE + machine_id})
              >> in(
                {_processors[machine_id].get(), artis::factory::Processor::inputs::IN_ITEM_AVAILABLE + storage.index});
          }
        }
        for (const auto &machine_id: graph_parameters.storage_machine_exchanges.ins.find(
          storage.index)->second.second) {
          if (_processors.find(machine_id) != _processors.end()) {
            out({_processors[machine_id].get(), artis::factory::Processor::outputs::OUT_ITEM + storage.index})
              >> in({_storages[storage.index].get(), artis::factory::Buffer::inputs::IN_ITEM + machine_id});
            out(
              {_processors[machine_id].get(), artis::factory::Processor::outputs::OUT_SPACE_AVAILABLE + storage.index})
              >> in({_storages[storage.index].get(), artis::factory::Buffer::inputs::IN_SPACE_AVAILABLE + machine_id});
            out({_storages[storage.index].get(), artis::factory::Buffer::outputs::OUT_SPACE_AVAILABLE + machine_id})
              >> in(
                {_processors[machine_id].get(), artis::factory::Processor::inputs::IN_SPACE_AVAILABLE + storage.index});
          }
        }
      }
    }

    // coordinator <-> external storages ports
    {
      std::vector<std::pair<uint, uint>> out_storages; // list of (machine,storage)
      std::vector<std::pair<uint, uint>> in_storages; // list of (machine,storage)

      for (const auto &e: graph_parameters.stocks.out_storages) {
        for (const auto &p: e.second.externals) {
          if (std::find(out_storages.begin(), out_storages.end(), std::make_pair(e.first, p)) == out_storages.end()) {
            out_storages.push_back(std::make_pair(e.first, p));
          }
        }
      }
      for (const auto &e: graph_parameters.stocks.in_storages) {
        for (const auto &p: e.second.externals) {
          if (std::find(in_storages.begin(), in_storages.end(), std::make_pair(e.first, p)) == in_storages.end()) {
            in_storages.push_back(std::make_pair(e.first, p));
          }
        }
      }
      for (const auto &[machine_id, storage_id]: out_storages) {
        coordinator->output_port(
          {outputs::OUT_ITEM + machine_id * 100 + storage_id,
           "out_item_" + std::to_string(machine_id) + "_" + std::to_string(storage_id)});
        coordinator->output_port(
          {outputs::OUT_SPACE_AVAILABLE + machine_id * 100 + storage_id,
           "out_space_available_" + std::to_string(machine_id) + "_" + std::to_string(storage_id)});
        coordinator->input_port(
          {inputs::IN_SPACE_AVAILABLE + machine_id * 100 + storage_id,
           "in_space_available_" + std::to_string(machine_id) + "_" + std::to_string(storage_id)});
      }
      for (const auto &[machine_id, storage_id]: in_storages) {
        coordinator->input_port(
          {inputs::IN_ITEM + machine_id * 100 + storage_id,
           "in_item_" + std::to_string(machine_id) + "_" + std::to_string(storage_id)});
        coordinator->output_port(
          {outputs::OUT_ITEM_AVAILABLE + machine_id * 100 + storage_id,
           "out_item_available_" + std::to_string(machine_id) + "_" + std::to_string(storage_id)});
        coordinator->input_port(
          {inputs::IN_ITEM_AVAILABLE + machine_id * 100 + storage_id,
           "in_item_available_" + std::to_string(machine_id) + "_" + std::to_string(storage_id)});
      }
    }

    // sub zones
    {
      for (const auto &p: graph_parameters.sub_zones) {
        ZoneGraphManagerParameters zone_parameters{p.second.id, p.first, p.second.sub_zones,
                                                   p.second.machines, p.second.in_outs,
                                                   p.second.stocks, p.second.storages, p.second.buffers,
                                                   graph_parameters.operation_durations,
                                                   graph_parameters.storage_machine_exchanges,
                                                   p.second.all_machines};

        _sub_zones[p.first] = std::make_unique<ZoneCoordinator>(p.second.id, common::NoParameters(), zone_parameters);
        this->add_child(sub_models::SUB_ZONE + p.first, _sub_zones[p.first].get());

        // connection sub zone <-> zone router
        out({_router.get(), artis::factory::ZoneRouter::outputs::OUT_SZ + p.first})
          >> in({_sub_zones[p.first].get(), artis::factory::Processor::inputs::IN_PO});
        out({_sub_zones[p.first].get(), artis::factory::Processor::outputs::OUT_PO})
          >> in({_router.get(), artis::factory::ZoneRouter::inputs::IN_SZ + p.first});
      }
    }

    // connection machine <-> external storages
    {
      for (const auto &m: graph_parameters.machines) {
        if (graph_parameters.stocks.out_storages.find(m.index) != graph_parameters.stocks.out_storages.end()) {
          for (const auto &e: graph_parameters.stocks.out_storages.find(m.index)->second.externals) {
            out({_processors[m.index].get(), artis::factory::Processor::outputs::OUT_ITEM + e})
              >> out({coordinator, outputs::OUT_ITEM + 100 * m.index + e});
            out({_processors[m.index].get(), artis::factory::Processor::outputs::OUT_SPACE_AVAILABLE + e})
              >> out({coordinator, outputs::OUT_SPACE_AVAILABLE + 100 * m.index + e});
            in({coordinator, inputs::IN_SPACE_AVAILABLE + 100 * m.index + e})
              >> in({_processors[m.index].get(), artis::factory::Processor::inputs::IN_SPACE_AVAILABLE + e});
          }
        }
        if (graph_parameters.stocks.in_storages.find(m.index) != graph_parameters.stocks.in_storages.end()) {
          for (const auto &e: graph_parameters.stocks.in_storages.find(m.index)->second.externals) {
            in({coordinator, inputs::IN_ITEM + 100 * m.index + e})
              >> in({_processors[m.index].get(), artis::factory::Processor::inputs::IN_ITEM + e});
            out({_processors[m.index].get(), artis::factory::Processor::outputs::OUT_ITEM_AVAILABLE + e})
              >> out({coordinator, outputs::OUT_ITEM_AVAILABLE + 100 * m.index + e});
            in({coordinator, inputs::IN_ITEM_AVAILABLE + 100 * m.index + e})
              >> in({_processors[m.index].get(), artis::factory::Processor::inputs::IN_ITEM_AVAILABLE + e});
          }
        }
      }
    }

    // connection sub zones <-> external storages
    {
      for (const auto &sz: graph_parameters.sub_zones) {
        for (const auto &m: graph_parameters.all_machines) {
          if (graph_parameters.stocks.out_storages.find(m.first) != graph_parameters.stocks.out_storages.end()) {
            for (const auto &e: graph_parameters.stocks.out_storages.find(m.first)->second.externals) {
              if (not exist_link(_sub_zones[m.second].get(),
                                 artis::factory::ZoneGraphManager::outputs::OUT_ITEM + m.first * 100 + e,
                                 common::InOutType::OUT,
                                 coordinator, outputs::OUT_ITEM + m.first * 100 + e, common::InOutType::OUT)) {
                out({_sub_zones[m.second].get(),
                     artis::factory::ZoneGraphManager::outputs::OUT_ITEM + m.first * 100 + e})
                  >> out({coordinator, outputs::OUT_ITEM + m.first * 100 + e});
                out({_sub_zones[m.second].get(),
                     artis::factory::ZoneGraphManager::outputs::OUT_SPACE_AVAILABLE + m.first * 100 + e})
                  >> out({coordinator, outputs::OUT_SPACE_AVAILABLE + m.first * 100 + e});
                in({coordinator, inputs::IN_SPACE_AVAILABLE + m.first * 100 + e})
                  >> in({_sub_zones[m.second].get(),
                         artis::factory::ZoneGraphManager::inputs::IN_SPACE_AVAILABLE + m.first * 100 + e});
              }
            }
          }
          if (graph_parameters.stocks.in_storages.find(m.first) != graph_parameters.stocks.in_storages.end()) {
            for (const auto &e: graph_parameters.stocks.in_storages.find(m.first)->second.externals) {
              if (not exist_link(coordinator, inputs::IN_ITEM + m.first * 100 + e, common::InOutType::IN,
                                 _sub_zones[m.second].get(),
                                 artis::factory::ZoneGraphManager::inputs::IN_ITEM + m.first * 100 + e,
                                 common::InOutType::IN)) {
                in({coordinator, inputs::IN_ITEM + m.first * 100 + e})
                  >> in({_sub_zones[m.second].get(),
                         artis::factory::ZoneGraphManager::inputs::IN_ITEM + m.first * 100 + e});
                out({_sub_zones[m.second].get(),
                     artis::factory::ZoneGraphManager::outputs::OUT_ITEM_AVAILABLE + m.first * 100 + e})
                  >> out({coordinator, outputs::OUT_ITEM_AVAILABLE + m.first * 100 + e});
                in({coordinator, inputs::IN_ITEM_AVAILABLE + m.first * 100 + e})
                  >> in({_sub_zones[m.second].get(),
                         artis::factory::ZoneGraphManager::inputs::IN_ITEM_AVAILABLE + m.first * 100 + e});
              }
            }
          }
        }
      }
    }

    // connection sub_zones <-> internal storages
    {
      for (const auto &m: graph_parameters.all_machines) {
        if (graph_parameters.stocks.out_storages.find(m.first) != graph_parameters.stocks.out_storages.end()) {
          for (const auto &e: graph_parameters.stocks.out_storages.find(m.first)->second.internals) {
            auto it_storage = _storages.find(e);
            auto it_sub_zone = _sub_zones.find(m.second);

            if (it_storage != _storages.end() and it_sub_zone != _sub_zones.end()) {
              out({it_sub_zone->second.get(),
                   artis::factory::ZoneGraphManager::outputs::OUT_ITEM + m.first * 100 + e})
                >> in({it_storage->second.get(), inputs::IN_ITEM + m.first});
              out({it_sub_zone->second.get(),
                   artis::factory::ZoneGraphManager::outputs::OUT_SPACE_AVAILABLE + m.first * 100 + e})
                >> in({it_storage->second.get(), inputs::IN_SPACE_AVAILABLE + m.first});
              out({it_storage->second.get(), outputs::OUT_SPACE_AVAILABLE + m.first})
                >> in({it_sub_zone->second.get(),
                       artis::factory::ZoneGraphManager::inputs::IN_SPACE_AVAILABLE + m.first * 100 + e});
            }
          }
        }
        if (graph_parameters.stocks.in_storages.find(m.first) != graph_parameters.stocks.in_storages.end()) {
          for (const auto &e: graph_parameters.stocks.in_storages.find(m.first)->second.internals) {
            auto it_storage = _storages.find(e);
            auto it_sub_zone = _sub_zones.find(m.second);

            if (it_storage != _storages.end() and it_sub_zone != _sub_zones.end()) {
              out({it_storage->second.get(), outputs::OUT_ITEM + m.first})
                >> in({it_sub_zone->second.get(),
                       artis::factory::ZoneGraphManager::inputs::IN_ITEM + m.first * 100 + e});
              out({it_sub_zone->second.get(),
                   artis::factory::ZoneGraphManager::outputs::OUT_ITEM_AVAILABLE + m.first * 100 + e})
                >> in({it_storage->second.get(), inputs::IN_ITEM_AVAILABLE + m.first});
              out({it_storage->second.get(), outputs::OUT_ITEM_AVAILABLE + m.first})
                >> in({it_sub_zone->second.get(),
                       artis::factory::ZoneGraphManager::inputs::IN_ITEM_AVAILABLE + m.first * 100 + e});
            }
          }
        }
      }
    }
  }

private:
  using ZoneRouterSimulator = artis::pdevs::Simulator<artis::common::DoubleTime, artis::factory::ZoneRouter, artis::factory::ZoneRouterParameters>;
  using ProcessorSimulator = artis::pdevs::Simulator<artis::common::DoubleTime, artis::factory::Processor, artis::factory::ProcessorParameters>;
  using SinkSimulator = artis::pdevs::Simulator<artis::common::DoubleTime, artis::factory::Sink, artis::factory::SinkParameters>;
  using BufferSimulator = artis::pdevs::Simulator<artis::common::DoubleTime, artis::factory::Buffer, artis::factory::BufferParameters>;
  using StorageSimulator = artis::pdevs::Simulator<artis::common::DoubleTime, artis::factory::Storage, artis::factory::StorageParameters>;
  using ZoneCoordinator = artis::pdevs::Coordinator<artis::common::DoubleTime, artis::factory::ZoneGraphManager, common::NoParameters, artis::factory::ZoneGraphManagerParameters>;

  std::unique_ptr<ZoneRouterSimulator> _router;
  std::map<uint, std::unique_ptr<ProcessorSimulator >> _processors;
  std::vector<std::unique_ptr<SinkSimulator >> _sinks;
  std::vector<std::unique_ptr<BufferSimulator >> _buffers;
  std::map<uint, std::unique_ptr<StorageSimulator >> _storages;
  std::map<uint, std::unique_ptr<ZoneCoordinator >> _sub_zones;
};

}

#endif //ARTIS_FACTORY_ZONE_GRAPH_MANAGER_HPP
