/**
 * @file Stock.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_FACTORY_STOCK_HPP
#define ARTIS_FACTORY_STOCK_HPP

#include "Base.hpp"
#include "ProductionOrder.hpp"

#include <memory>

namespace artis::factory {

struct ProductionOrderData {
  unsigned int po_id;
  unsigned int po_index;
};

struct ItemData {
  unsigned int item_id;
  double quantity;
  std::unique_ptr<ProductionOrderData> po_data;

  ItemData() {}

  ItemData(unsigned int item_id, double quantity)
    : item_id(item_id), quantity(quantity), po_data(nullptr) {}

  ItemData(unsigned int item_id, double quantity, unsigned int po_id, unsigned int po_index)
    : item_id(item_id), quantity(quantity),
      po_data(std::make_unique<ProductionOrderData>(ProductionOrderData{po_id, po_index})) {}

  ItemData(const ItemData &other) : item_id(other.item_id), quantity(other.quantity),
                                    po_data(other.po_data ? new ProductionOrderData{other.po_data->po_id,
                                                                                    other.po_data->po_index}
                                                          : nullptr) {
  }

  ItemData &operator=(const ItemData &other) {
    item_id = other.item_id;
    quantity = other.quantity;
    if (other.po_data) {
      po_data = std::make_unique<ProductionOrderData>(
        ProductionOrderData{other.po_data->po_id, other.po_data->po_index});
    } else {
      po_data.reset();
    }
    return *this;
  }

};

struct ItemDemand : ItemData {
  ItemDemand() {}

  ItemDemand(unsigned int item_id, double quantity, unsigned int po_id, unsigned int po_index)
    : ItemData(item_id, quantity, po_id, po_index) {}

  ItemDemand(const ItemDemand &) = default;

  bool operator==(const ItemDemand &other) const {
    return po_data->po_id == other.po_data->po_id and po_data->po_index == other.po_data->po_index and
           item_id == other.item_id and quantity == other.quantity;
  }

  std::string to_string() const {
    return "ItemDemand < " + std::to_string(po_data->po_id) + " | " + std::to_string(po_data->po_index) + " | " +
           std::to_string(item_id) + " | " + std::to_string(quantity) + " >";
  }
};

struct ItemInput : ItemData {
  ItemInput() {}

  ItemInput(unsigned int item_id, double quantity) : ItemData(item_id, quantity) {}

  ItemInput(unsigned int item_id, double quantity, unsigned int po_id, unsigned int po_index)
    : ItemData(item_id, quantity, po_id, po_index) {}

  ItemInput(const ItemInput &) = default;

  bool operator==(const ItemInput &other) const {
    return ((po_data == nullptr and other.po_data == nullptr) or
            (po_data->po_id == other.po_data->po_id and po_data->po_index == other.po_data->po_index)) and
           item_id == other.item_id and quantity == other.quantity;
  }

  std::string to_string() const {
    return "ItemInput < " + (po_data ? std::to_string(po_data->po_id) : "null") + " | " +
           (po_data ? std::to_string(po_data->po_index) : "null") + " | " +
           std::to_string(item_id) + " | " + std::to_string(quantity) + " >";
  }
};

struct ItemSpace : ItemData {
  ItemSpace() {}

  ItemSpace(unsigned int item_id, double quantity) : ItemData(item_id, quantity) {}

  ItemSpace(unsigned int item_id, double quantity, unsigned int po_id, unsigned int po_index)
    : ItemData(item_id, quantity, po_id, po_index) {}

  ItemSpace(const ItemSpace &) = default;

  bool operator==(const ItemInput &other) const {
    return ((po_data == nullptr and other.po_data == nullptr) or
            (po_data->po_id == other.po_data->po_id and po_data->po_index == other.po_data->po_index)) and
           item_id == other.item_id and quantity == other.quantity;
  }

  std::string to_string() const {
    return "ItemSpace < " + (po_data ? std::to_string(po_data->po_id) : "null") + " | " +
           (po_data ? std::to_string(po_data->po_index) : "null") + " | " +
           std::to_string(item_id) + " | " + std::to_string(quantity) + " >";
  }
};

struct StockAvailableResponse {
  int po_id;

  enum values {
    AVAILABLE, NOT_AVAILABLE, NEW_AVAILABLE
  } response;

  StockAvailableResponse(unsigned int po_id = 0, values response = NOT_AVAILABLE)
    : po_id(po_id), response(response) {}

  bool operator==(const StockAvailableResponse &other) const {
    return po_id == other.po_id and response == other.response;
  }

  std::string to_string() const {
    return "StockAvailableResponse < " + std::to_string(po_id) + " | " +
           (response == AVAILABLE ? "AVAILABLE" : (response == NOT_AVAILABLE ? "NOT AVAILABLE" : "NEW AVAILABLE")) +
           " >";
  }
};

struct ProductResponse {
  int product_id;
  int po_id;

  ProductResponse(int product_id = -1, int po_id = -1) : product_id(product_id), po_id(po_id) {}

  bool operator==(const ProductResponse &other) const {
    return product_id == other.product_id and po_id == other.po_id;
  }

  std::string to_string() const {
    return "Response < " + std::to_string(product_id) + " | " + std::to_string(po_id) + " >";
  }
};

struct StockParameters {
  std::vector<unsigned int> _in_machines;
  std::vector<unsigned int> _out_machines;
  int _capacity; // if -1 then infinity
};

template<typename Model, typename Parameters>
class Stock : public Dynamics<Model, Parameters> {
public:
  struct inputs {
    enum values {
      IN_ITEM = 1000, IN_ITEM_AVAILABLE = 2000, IN_SPACE_AVAILABLE = 3000
    };
  };

  struct outputs {
    enum values {
      OUT_ITEM = 1000, OUT_ITEM_AVAILABLE = 2000, OUT_SPACE_AVAILABLE = 3000
    };
  };

  struct vars {
    enum values {
      ENTRY_NUMBER
    };
  };

  Stock(const std::string &name, const Context<Model, Parameters> &context)
    : Dynamics<Model, Parameters>(name, context), _parameters(context.parameters()) {
    for (const auto &m: _parameters._in_machines) {
      this->input_port({inputs::IN_ITEM + m, "in_item_" + std::to_string(m)});
      this->input_port({inputs::IN_SPACE_AVAILABLE + m, "in_space_available_" + std::to_string(m)});
      this->output_port({outputs::OUT_SPACE_AVAILABLE + m, "out_space_available_" + std::to_string(m)});
    }
    for (const auto &m: _parameters._out_machines) {
      this->input_port({inputs::IN_ITEM_AVAILABLE + m, "in_item_available_" + std::to_string(m)});
      this->output_port({outputs::OUT_ITEM + m, "out_item_" + std::to_string(m)});
      this->output_port({outputs::OUT_ITEM_AVAILABLE + m, "out_item_available_" + std::to_string(m)});
    }
    this->observables({{vars::ENTRY_NUMBER, "entry number"}});
  }

  ~Stock() override = default;

  void dint(const Time &t) override;

  void dext(const Time &t, const Time &e, const Bag &bag) override;

  void start(const Time &t) override;

  Time ta(const Time &t) const override;

  Bag lambda(const Time &t) const override;

  artis::common::event::Value observe(const Time &t, unsigned int index) const override;

private:
  struct Phase {
    enum values {
      WAIT,
      SEND,
      SEND_AVAILABLE
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case WAIT:
          return "WAIT";
        case SEND:
          return "SEND";
        case SEND_AVAILABLE:
          return "SEND AVAILABLE";
      }
      return "";
    }
  };

  struct Entry : ItemData {
    Entry(unsigned int item_id, double quantity, Time t) : ItemData(item_id, quantity), time(t) {}

    Entry(unsigned int item_id, double quantity, unsigned int po_id, unsigned int po_index, Time t) : ItemData(
      item_id, quantity, po_id, po_index), time(t) {}

    Time time;
  };

  struct AvailableDemand {
    unsigned int _machine_id;
    unsigned int _po_id;
    bool _available;
  };

  Parameters _parameters;

  typename Phase::values _phase;
  std::vector<std::pair<unsigned int, ItemDemand>> _satisfied_item_demands;
  std::vector<std::pair<unsigned int, ItemDemand>> _unsatisfied_item_demands;

  std::vector<AvailableDemand> _available_demands;
  std::deque<std::pair<unsigned int, unsigned int>> _waiting_machine_ids; // pair(machine_id,po_id)

protected:
  std::vector<Entry> _entries;
};

} // namespace artis::factory

#include "Stock.tpp"

#endif