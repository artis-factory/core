/**
 * @file Processor.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Buffer.hpp"
#include "Processor.hpp"
#include "Stock.hpp"

namespace artis::factory {

void Processor::dint(const Time &t) {
  for (auto it = _jobs.cbegin(); it != _jobs.cend();) {
    if (it->second._next_time == t and it->second._phase == Phase::READY) {
      it = _jobs.erase(it);
    } else {
      ++it;
    }
  }
  for (auto &j: _jobs) {
    auto &job = j.second;

    if (job._next_time == t) {
      switch (job._phase) {
        case Phase::ITEM_DEMAND:
          job._phase = Phase::WAIT_SUPPLY;
          job._next_time = artis::common::DoubleTime::infinity;
          break;
        case Phase::WAIT_SUPPLY:
        case Phase::WAIT:
          assert(false);
          break;
        case Phase::LOADING: {
          job._phase = Phase::PROCESSING;
          job._next_time = t + durations(job._po->get_productID(), job._po->operation_index()).processing_time;
          break;
        }
        case Phase::PROCESSING: {
          job._phase = Phase::UNLOADING;
          job._next_time = t + durations(job._po->get_productID(), job._po->operation_index()).unloading_time;
          break;
        }
        case Phase::UNLOADING: {
          auto it = _parameters.product_in_outs.find(job._po->get_productID());

          if (it != _parameters.product_in_outs.cend()) {
            if (not it->second.find(_parameters.machine_id)->second.storage_in_outs.second.empty()) {
              job._phase = Phase::SEND_STOCK_AVAILABLE;
              job._next_time = t;
            }
            if (not it->second.find(_parameters.machine_id)->second.buffer_in_outs.second.empty()) {
              job._phase = Phase::SEND_STOCK_AVAILABLE;
              job._next_time = t;
            }
          } else {
            job._phase = Phase::READY;
            job._next_time = t;
          }

#ifdef WITH_TRACE
          Trace::trace()
            << TraceElement(get_full_name(), t,
                            artis::common::FormalismType::PDEVS,
                            artis::common::FunctionType::DELTA_INT,
                            artis::common::LevelType::USER)
            << "FINISH po = " << job._po->to_string();
          Trace::trace().flush();
#endif

          break;
        }
        case Phase::SEND_STOCK_AVAILABLE: {
          job._phase = Phase::WAIT_STOCK_AVAILABLE;
          job._next_time = artis::common::DoubleTime::infinity;
          break;
        }
        default:
          assert(false);
      }
    }
  }
}

void Processor::dext(const Time &t, const Time & /* e */, const Bag &bag) {
  std::for_each(bag.begin(), bag.end(), [this, t](const ExternalEvent &event) {
    if (event.on_port(inputs::IN_PO)) {
      uint8_t *data = nullptr;

      assert(_jobs.size() < _parameters.capacity);

      event.data()(data);

      auto po = std::make_unique<ProductionOrder>(data, event.data().size());
      auto po_id = po->getID();
      auto product_id = po->get_productID();
      auto it = _parameters.product_in_outs.find(po->get_productID());
      auto operation_index = po->operation_index();

      if (it == _parameters.product_in_outs.end() or
          (it->second.find(_parameters.machine_id)->second.storage_in_outs.first.empty() and
           it->second.find(_parameters.machine_id)->second.buffer_in_outs.first.empty())) {
        _jobs[po_id] = Job{Phase::LOADING, std::move(po), 0, t + durations(product_id, operation_index).loading_time};
      } else {
        unsigned int demand_number = it->second.find(_parameters.machine_id)->second.buffer_in_outs.first.size() +
                                     it->second.find(_parameters.machine_id)->second.storage_in_outs.first.size();

        _jobs[po_id] = Job{Phase::ITEM_DEMAND, std::move(po), demand_number, t};
      }

#ifdef WITH_TRACE
      Trace::trace()
        << TraceElement(get_full_name(), t,
                        artis::common::FormalismType::PDEVS,
                        artis::common::FunctionType::DELTA_EXT,
                        artis::common::LevelType::USER)
        << "START po = " << _jobs[po_id]._po->to_string();
      Trace::trace().flush();
#endif

    } else if (event.port_index() >= inputs::IN_ITEM and event.port_index() < inputs::IN_ITEM_AVAILABLE) {
      ProductResponse response;

      event.data()(response);

      assert(_jobs.find(response.po_id) != _jobs.end());

      auto &job = _jobs[response.po_id];

      job._demand_number--;
      if (job._demand_number == 0) {
        job._phase = Phase::LOADING;
        job._next_time = t + durations(job._po->get_productID(), job._po->operation_index()).loading_time;
      }

#ifdef WITH_TRACE
      Trace::trace()
        << TraceElement(get_full_name(), t,
                        artis::common::FormalismType::PDEVS,
                        artis::common::FunctionType::DELTA_EXT,
                        artis::common::LevelType::USER)
        << "IN ITEM item = " << response.to_string();
      Trace::trace().flush();
#endif

    } else if (event.port_index() >= inputs::IN_ITEM_AVAILABLE and event.port_index() < inputs::IN_SPACE_AVAILABLE) {
      ProductResponse response;

      event.data()(response);

      assert(_jobs.find(response.po_id) != _jobs.end());

      auto &job = _jobs[response.po_id];

      job._demand_number--;
      if (job._demand_number == 0) {
        job._phase = Phase::LOADING;
        job._next_time = t + durations(job._po->get_productID(), job._po->operation_index()).loading_time;
      }

#ifdef WITH_TRACE
      Trace::trace()
        << TraceElement(get_full_name(), t,
                        artis::common::FormalismType::PDEVS,
                        artis::common::FunctionType::DELTA_EXT,
                        artis::common::LevelType::USER)
        << "IN ITEM AVAILABLE item = " << response.to_string();
      Trace::trace().flush();
#endif

    } else if (event.port_index() >= inputs::IN_SPACE_AVAILABLE) {
      StockAvailableResponse response;

      event.data()(response);

      assert(_jobs.find(response.po_id) != _jobs.end());

      auto &job = _jobs[response.po_id];

      switch (response.response) {
        case StockAvailableResponse::AVAILABLE: {
          job._phase = Phase::READY;
          job._next_time = t;
          break;
        }
        case StockAvailableResponse::NOT_AVAILABLE: {
          job._phase = Phase::WAIT_STOCK_AVAILABLE;
          job._next_time = artis::common::DoubleTime::infinity;
          break;
        }
        case StockAvailableResponse::NEW_AVAILABLE: {
          job._phase = Phase::SEND_STOCK_AVAILABLE;
          job._next_time = t;
          break;
        }
      }

#ifdef WITH_TRACE
      Trace::trace()
        << TraceElement(get_name(), t,
                        artis::common::FormalismType::PDEVS,
                        artis::common::FunctionType::DELTA_EXT,
                        artis::common::LevelType::USER)
        << "IN SPACE AVAILABLE item = " << response.to_string();
      Trace::trace().flush();
#endif

    }
  });
}

void Processor::start(const Time & /* t */) {
}

Time Processor::ta(const Time &t) const {
  if (not _jobs.empty()) {
    return std::min_element(_jobs.cbegin(), _jobs.cend(), [](const auto &lhs, const auto &rhs) {
      return (lhs.second._next_time != artis::common::DoubleTime::infinity and
              lhs.second._next_time < rhs.second._next_time) or
             rhs.second._next_time == artis::common::DoubleTime::infinity;
    })->second._next_time - t;
  }
  return artis::common::DoubleTime::infinity;
}

Bag Processor::lambda(const Time &t) const {
  Bag bag;

  for (const auto &e: _jobs) {
    const auto &job = e.second;

    if (job._next_time == t) {
      if (job._phase == Phase::READY) {
        bag.push_back(ExternalEvent(outputs::OUT_PO, common::event::Value(job._po->_buffer, job._po->_size)));

#ifdef WITH_TRACE
        Trace::trace()
          << TraceElement(get_full_name(), t,
                          artis::common::FormalismType::PDEVS,
                          artis::common::FunctionType::LAMBDA,
                          artis::common::LevelType::USER)
          << "SEND po = " << job._po->to_string();
        Trace::trace().flush();
#endif

        auto it = _parameters.product_in_outs.find(job._po->get_productID());

        if (it != _parameters.product_in_outs.cend()) {
          for (const auto &p: it->second.find(_parameters.machine_id)->second.buffer_in_outs.second) {
            bag.push_back(ExternalEvent(outputs::OUT_ITEM + p.buffer_ID,
                                        ItemInput(p.item_ID, p.quantity, e.first, job._po->operation_index())));

#ifdef WITH_TRACE
            Trace::trace()
              << TraceElement(get_full_name(), t,
                              artis::common::FormalismType::PDEVS,
                              artis::common::FunctionType::LAMBDA,
                              artis::common::LevelType::USER)
              << "SEND ITEM = " << p.item_ID << " with quantity " << p.quantity << " to buffer " << p.buffer_ID;
            Trace::trace().flush();
#endif

          }
          for (const auto &p: it->second.find(_parameters.machine_id)->second.storage_in_outs.second) {
            bag.push_back(ExternalEvent(outputs::OUT_ITEM + p.storage_ID,
                                        ItemInput(p.item_ID, p.quantity, e.first, job._po->operation_index())));

#ifdef WITH_TRACE
            Trace::trace()
              << TraceElement(get_full_name(), t,
                              artis::common::FormalismType::PDEVS,
                              artis::common::FunctionType::LAMBDA,
                              artis::common::LevelType::USER)
              << "SEND ITEM = " << p.item_ID << " with quantity " << p.quantity << " to storage " << p.storage_ID;
            Trace::trace().flush();
#endif

          }
        }
      } else if (job._phase == Phase::ITEM_DEMAND) {
        auto it = _parameters.product_in_outs.find(job._po->get_productID())->second.find(_parameters.machine_id);

        for (const auto &item: it->second.buffer_in_outs.first) {
          bag.push_back(
            ExternalEvent(outputs::OUT_ITEM_AVAILABLE + item.buffer_ID,
                          ItemDemand(item.item_ID, item.quantity, e.first, job._po->operation_index())));

#ifdef WITH_TRACE
          Trace::trace()
            << TraceElement(get_full_name(), t,
                            artis::common::FormalismType::PDEVS,
                            artis::common::FunctionType::LAMBDA,
                            artis::common::LevelType::USER)
            << "SEND ITEM DEMAND = " << item.item_ID << " with quantity " << item.quantity << " to buffer "
            << item.buffer_ID;
          Trace::trace().flush();
#endif

        }
        for (const auto &item: it->second.storage_in_outs.first) {
          bag.push_back(
            ExternalEvent(outputs::OUT_ITEM_AVAILABLE + item.storage_ID,
                          ItemDemand(item.item_ID, item.quantity, e.first, job._po->operation_index())));

#ifdef WITH_TRACE
          Trace::trace()
            << TraceElement(get_full_name(), t,
                            artis::common::FormalismType::PDEVS,
                            artis::common::FunctionType::LAMBDA,
                            artis::common::LevelType::USER)
            << "SEND ITEM DEMAND = " << item.item_ID << " with quantity " << item.quantity << " to storage "
            << item.storage_ID;
          Trace::trace().flush();
#endif

        }
      } else if (job._phase == Phase::SEND_STOCK_AVAILABLE) {
        auto it = _parameters.product_in_outs.find(job._po->get_productID())->second.find(_parameters.machine_id);

        for (const auto &item: it->second.storage_in_outs.second) {
          bag.push_back(ExternalEvent(outputs::OUT_SPACE_AVAILABLE + item.storage_ID, e.first));

#ifdef WITH_TRACE
          Trace::trace()
            << TraceElement(get_full_name(), t,
                            artis::common::FormalismType::PDEVS,
                            artis::common::FunctionType::LAMBDA,
                            artis::common::LevelType::USER)
            << "SEND SPACE AVAILABLE = " << item.item_ID << " with quantity " << item.quantity << " to storage "
            << item.storage_ID;
          Trace::trace().flush();
#endif

        }
        for (const auto &item: it->second.buffer_in_outs.second) {
          bag.push_back(ExternalEvent(outputs::OUT_SPACE_AVAILABLE + item.buffer_ID, e.first));

#ifdef WITH_TRACE
          Trace::trace()
            << TraceElement(get_full_name(), t,
                            artis::common::FormalismType::PDEVS,
                            artis::common::FunctionType::LAMBDA,
                            artis::common::LevelType::USER)
            << "SEND SPACE AVAILABLE = " << item.item_ID << " with quantity " << item.quantity << " to buffer "
            << item.buffer_ID;
          Trace::trace().flush();
#endif

        }
      }
    }
  }
  return bag;
}

artis::common::event::Value Processor::observe(const Time & /* t */, unsigned int /* index */) const {
  return {};
}

} // namespace artis::factory