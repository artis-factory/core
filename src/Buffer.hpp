/**
 * @file Buffer.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_FACTORY_BUFFER_HPP
#define ARTIS_FACTORY_BUFFER_HPP

#include "Base.hpp"
#include "ProductionOrder.hpp"
#include "Stock.hpp"

namespace artis::factory {

//struct ItemDemand {
//  int item_id;
//  int po_id;
//  unsigned int quantity;
//
//  ItemDemand(int item_id = -1, int po_id = -1, unsigned int quantity = 0) : item_id(item_id), po_id(po_id),
//                                                                            quantity(quantity) {}
//
//  bool operator==(const ItemDemand &other) const {
//    return item_id == other.item_id and po_id == other.po_id and quantity == other.quantity;
//  }
//
//  std::string to_string() const {
//    return "Demand < " + std::to_string(item_id) + " | " + std::to_string(po_id) + " | " + std::to_string(quantity) +
//           " >";
//  }
//};
//
//struct ItemResponse {
//  int item_id;
//  int po_id;
//
//  ItemResponse(int item_id = -1, int po_id = -1) : item_id(item_id), po_id(po_id) {}
//
//  bool operator==(const ItemResponse &other) const {
//    return item_id == other.item_id and po_id == other.po_id;
//  }
//
//  std::string to_string() const {
//    return "Response < " + std::to_string(item_id) + " | " + std::to_string(po_id) + " >";
//  }
//};

struct BufferParameters : StockParameters {};

class Buffer : public Stock<Buffer, BufferParameters> {
public:
  Buffer(const std::string &name, const Context<Buffer, BufferParameters> &context)
    : Stock<Buffer, BufferParameters>(name, context), _parameters(context.parameters()) {
  }

  ~Buffer() override = default;

//  void dint(const Time &t) override;
//
//  void dext(const Time &t, const Time &e, const Bag &bag) override;
//
//  void start(const Time &t) override;
//
//  Time ta(const Time &t) const override;
//
//  Bag lambda(const Time &t) const override;
//
//  artis::common::event::Value observe(const Time &t, unsigned int index) const override;

private:
  BufferParameters _parameters;
};

} // namespace artis::factory

#endif