/**
 * @file Buffer.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Buffer.hpp"

namespace artis::factory {

//void Buffer::dint(const Time & /* t */) {
//  switch (_phase) {
//    case Phase::SEND: {
//      _demands.clear();
//      _phase = Phase::WAIT;
//      break;
//    }
//    case Phase::WAIT: {
//      assert(false);
//    }
//  }
//}
//
//void Buffer::dext(const Time & /* t */, const Time & /* e */, const Bag &bag) {
//  std::for_each(bag.begin(), bag.end(), [this](const ExternalEvent &event) {
//    ItemDemand demand;
//
//    event.data()(demand);
//    if (_items[demand.item_id] == -1 or _items[demand.item_id] >= (int) demand.quantity) {
//      _demands.push_back(std::make_pair(event.port_index() - inputs::IN, demand));
//      if (_items[demand.item_id] != -1) {
//        _items[demand.item_id] -= (int) demand.quantity;
//      }
//      _phase = Phase::SEND;
//    } else {
//      _unsatisfied_demands.push_back(std::make_pair(event.port_index() - inputs::IN, demand));
//    }
//  });
//}
//
//void Buffer::start(const Time & /* t */) {
//  _phase = Phase::WAIT;
//  _items = _parameters._items;
//}
//
//Time Buffer::ta(const Time & /* t */) const {
//  switch (_phase) {
//    case Phase::WAIT:
//      return artis::common::DoubleTime::infinity;
//    case Phase::SEND:
//      return 0;
//  }
//  return artis::common::DoubleTime::infinity;
//}
//
//Bag Buffer::lambda(const Time & /* t */) const {
//  Bag bag;
//
//  if (_phase == Phase::SEND) {
//    for (const auto &e: _demands) {
//      bag.push_back(ExternalEvent(outputs::OUT + e.first, ItemResponse(e.second.item_id, e.second.po_id)));
//    }
//  }
//  return bag;
//}
//
//artis::common::event::Value Buffer::observe(const Time & /* t */, unsigned int /* index */) const {
//  return {};
//}

} // namespace artis::factory