/**
 * @file Model.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_FACTORY_DSL_TO_DEVS_MODEL_HPP
#define ARTIS_FACTORY_DSL_TO_DEVS_MODEL_HPP

#include "ProductionOrder.hpp"

#include <map>

namespace artis::factory::dsl_to_devs {

using MachineIndex = uint;

using StockIndex = uint;

using ProductIndex = uint;

using OperationIndex = size_t;

struct StorageMachineExchanges {
  using ZoneID = uint;
  using MachineID = uint;
  using StorageID = uint;
  using Machines = std::pair<std::vector<ZoneID>, std::vector<MachineID>>; // zone, machine

  std::map<StorageID, Machines> ins; // machine -> storage (in)
  std::map<StorageID, Machines> outs; // storage -> machine (out)
};

struct BufferFlow {
  uint item_ID;
  uint buffer_ID;
  double quantity;
};

struct StorageFlow {
  uint item_ID;
  uint storage_ID;
  double quantity;
};

using BufferFlows = std::vector<BufferFlow>;
using StorageFlows = std::vector<StorageFlow>;

struct InOuts {
  std::pair<BufferFlows, BufferFlows> buffer_in_outs;
  std::pair<StorageFlows, StorageFlows> storage_in_outs;
};

using ProductInOuts = std::map<ProductIndex, std::map<MachineIndex, InOuts>>;

struct Durations {
};

struct ProcessorDurations : Durations {
  ProcessorDurations(uint loading_time, uint setup_time, uint processing_time, uint unloading_time) : loading_time(
    loading_time), setup_time(setup_time), processing_time(processing_time), unloading_time(unloading_time) {}

  uint loading_time;
  uint setup_time;
  uint processing_time;
  uint unloading_time;
};

using OperationDurations = std::map<OperationIndex, ProcessorDurations>;

using ProductOperationDurations = std::map<ProductIndex, OperationDurations>;

using MachineProductOperationDurations = std::map<MachineIndex, ProductOperationDurations>;

struct Machine {
  std::string id;
  uint index;
  uint capacity;
  enum MachineSubType {
    UNDEFINED, PROCESSOR, ROBOT, COMBINER, SEPARATOR, MULTI_PROCESSOR
  } type;
  uint setup_time;
  uint loading_time;
  uint processing_time;
  uint unloading_time;
};

using Machines = std::vector<Machine>;

struct Zone;

using Zones = std::map<uint, Zone>;

struct Stocks {
  struct Storages {
    std::vector<StockIndex> internals;
    std::vector<StockIndex> externals;
  };

  std::map<MachineIndex, Storages> in_storages; // machine -> storage (in)
  std::map<MachineIndex, Storages> out_storages; // machine -> storage (out)

  struct MachineBuffers {
    std::vector<uint> ins;
    std::vector<uint> outs;
  };

  struct BufferMachines {
    std::vector<uint> ins;
    std::vector<uint> outs;
  };

  std::map<uint, MachineBuffers> machine_buffers; // machine -> buffers (in / out)
  std::map<uint, BufferMachines> buffer_machines; // buffer -> machines (in / out)
};

struct Buffer {
  std::string id;
  uint index;
  int capacity; // -1 if infinity
};

using Buffers = std::vector<Buffer>;

struct Storage {
  std::string id;
  uint index;
  int capacity; // -1 if infinity
  std::map<uint, double> initial_quantities;
};

using Storages = std::vector<Storage>;

struct Zone {
  std::string id;
  uint index;
  Zones sub_zones;
  Machines machines;
  ProductInOuts in_outs;
  Stocks stocks;
  Storages storages;
  Buffers buffers;
  StorageMachineExchanges storage_machine_exchanges;
  std::vector<std::pair<uint, uint>> all_machines;
};

struct Factory {
  StorageMachineExchanges storage_machine_exchanges;
  Programs programs;
  Zones zones;
  MachineProductOperationDurations durations;
  Storages storages;
  Machines machines;
};

}

#endif //ARTIS_FACTORY_DSL_TO_DEVS_MODEL_HPP
