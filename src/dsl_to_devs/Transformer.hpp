/**
 * @file dsl_to_devs/Transformer.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_FACTORY_DSL_TO_DEVS_TRANSFORMER_HPP
#define ARTIS_FACTORY_DSL_TO_DEVS_TRANSFORMER_HPP

#include "dsl_to_devs/Model.hpp"
#include "json_dsl/model/Model.hpp"

namespace artis::factory::dsl_to_devs {

struct Transformer {

  enum InZoneResult {
    NOT_FOUND, LOCAL_FOUND, SUB_ZONE_FOUND
  };

  std::vector<uint> build_path(const json_dsl::Factory &json_factory, const std::string &zone_ID) const {
    std::vector<uint> path;

    if (json_factory.zones.find(zone_ID) == json_factory.zones.cend()) {
      for (const auto e: json_factory.all_zones.find(zone_ID)->second.path) {
        path.push_back(json_factory.all_zones.find(e)->second.index);
      }
    }
    return path;
  }

  Programs build_programs(const json_dsl::Factory &json_factory) const {
    Programs programs;

    for (const auto &product: json_factory.catalog) {
      auto product_id = product.second.first;
      Program program;
      auto op = product.second.second->program.first_operation;

      if (op) {
        uint current_zone_id = json_factory.all_zones.find(op->zone_ID->ID)->second.index;
        std::vector<uint> path = build_path(json_factory, op->zone_ID->ID);
        std::vector<uint8_t> machines;

        while (op) {
          machines.push_back(json_factory.all_machines.find(op->machine_ID->ID)->second.first);
          op = op->next_operation;
          if (op and json_factory.all_zones.find(op->zone_ID->ID)->second.index != current_zone_id) {
            for (const auto &e: path) {
              program.push_back({(uint8_t) e, {}});
            }
            program.push_back({(uint8_t) current_zone_id, machines});
            current_zone_id = json_factory.all_zones.find(op->zone_ID->ID)->second.index;
            path = build_path(json_factory, op->zone_ID->ID);
            machines.clear();
          }
        }
        if (not machines.empty()) {
          for (const auto &e: path) {
            program.push_back({(uint8_t) e, {}});
          }
          program.push_back({(uint8_t) current_zone_id, machines});
        }
      }
      programs[product_id] = program;
    }
    return programs;
  }

  std::vector<std::pair<uint, uint>>
  get_all_machines(const json_dsl::Zone &json_zone, const json_dsl::Factory &json_factory) const {
    std::vector<std::pair<uint, uint>> machines;

    for (const auto &m: json_factory.all_machines) {
      auto result = machine_in_zone(json_zone, m.first);

      if (result == InZoneResult::LOCAL_FOUND or result == InZoneResult::SUB_ZONE_FOUND) {
        const auto *z = zone_of_machine(m.first, json_factory);

        machines.push_back(std::make_pair(m.second.first, json_factory.all_zones.find(z->ID)->second.index));
      }
    }
    return machines;
  }

  Buffers get_buffers(const json_dsl::Zone &json_zone, const json_dsl::Factory &json_factory) const {
    Buffers buffers;

    for (const auto &s: json_zone.buffers) {
      Buffer buffer;

      buffer.id = s.second->ID;
      buffer.index = json_factory.all_buffers.find(s.second->ID)->second.first;
      buffer.capacity = s.second->capacity;
      buffers.push_back(buffer);
    }
    return buffers;
  }

  ProductInOuts get_flows(const json_dsl::Zone &zone, const json_dsl::Factory &json_factory) const {
    ProductInOuts in_outs;

    for (const auto &product: json_factory.catalog) {
      auto productID = product.second.first;
      auto op = product.second.second->program.first_operation;

      if (op) {
        while (op) {
          if (zone.machines.find(op->machine_ID->ID) != zone.machines.end()) {
            uint machine_index = json_factory.all_machines.find(op->machine_ID->ID)->second.first;

            if (in_outs.find(productID) == in_outs.end()) {
              in_outs[productID] = std::map<uint, InOuts>();
            }
            if (in_outs[productID].find(machine_index) == in_outs[productID].end()) {
              in_outs[productID][machine_index] = InOuts();
            }
            for (const auto &part: op->out) {
              if (part.storage) {
                in_outs[productID][machine_index].storage_in_outs.second.push_back(
                  StorageFlow{json_factory.all_items.find(part.item->id)->second.first,
                              json_factory.all_storages.find(part.storage->ID)->second.first,
                              part.quantity});
              } else { // buffer
                in_outs[productID][machine_index].buffer_in_outs.second.push_back(
                  BufferFlow{json_factory.all_items.find(part.item->id)->second.first,
                             json_factory.all_buffers.find(part.buffer->ID)->second.first,
                             part.quantity});
              }
            }
            for (const auto &part: op->in) {
              if (part.storage) {
                in_outs[productID][machine_index].storage_in_outs.first.push_back(
                  StorageFlow{json_factory.all_items.find(part.item->id)->second.first,
                              json_factory.all_storages.find(part.storage->ID)->second.first,
                              part.quantity});
              } else { // buffer
                in_outs[productID][machine_index].buffer_in_outs.first.push_back(
                  BufferFlow{json_factory.all_items.find(part.item->id)->second.first,
                             json_factory.all_buffers.find(part.buffer->ID)->second.first,
                             part.quantity});
              }
            }
          }
          op = op->next_operation;
        }
      }
    }
    return in_outs;
  }

  Machines get_machines(const json_dsl::Factory &json_factory) const {
    Machines machines;

    for (const auto &m: json_factory.all_machines) {
      const auto &p = m.second.second;

      machines.push_back(
        Machine{m.first, m.second.first, p->capacity, (Machine::MachineSubType) p->type, p->setup_time, p->loading_time,
                p->processing_time,
                p->unloading_time});
    }
    return machines;
  }

  Machines get_machines(const json_dsl::Zone &json_zone, const json_dsl::Factory &json_factory) const {
    Machines machines;

    for (const auto &m: json_zone.machines) {
      const auto &p = m.second;

      machines.push_back(Machine{m.first, json_factory.all_machines.find(m.first)->second.first,
                                 p->capacity, (Machine::MachineSubType) p->type, p->setup_time, p->loading_time,
                                 p->processing_time, p->unloading_time});
    }
    return machines;
  }

  Stocks get_stocks(const json_dsl::Zone &json_zone, const json_dsl::Factory &json_factory) const {
    Stocks stocks;

    for (const auto &product: json_factory.catalog) {
      auto op = product.second.second->program.first_operation;

      if (op) {
        while (op) {
          InZoneResult result = machine_in_zone(json_zone, op->machine_ID->ID);

          if (result != InZoneResult::NOT_FOUND) {
            uint machine_id = json_factory.all_machines.find(op->machine_ID->ID)->second.first;

            for (const auto &part: op->out) {
              if (part.storage) {
                uint storageID = json_factory.all_storages.find(part.storage->ID)->second.first;

                if (stocks.out_storages.find(machine_id) == stocks.out_storages.end()) {
                  stocks.out_storages[machine_id] = Stocks::Storages();
                }
                insert_storage(storage_in_zone(json_zone, part.storage->ID), storageID,
                               stocks.out_storages[machine_id]);
              } else if (part.buffer and result == InZoneResult::LOCAL_FOUND) { // buffer
                uint bufferID = json_factory.all_buffers.find(part.buffer->ID)->second.first;

                if (stocks.machine_buffers.find(machine_id) == stocks.machine_buffers.end()) {
                  stocks.machine_buffers[machine_id] = Stocks::MachineBuffers();
                }
                insert_buffer(json_zone, part.buffer->ID, bufferID, stocks.machine_buffers[machine_id].outs);
                if (stocks.buffer_machines.find(bufferID) == stocks.buffer_machines.end()) {
                  stocks.buffer_machines[bufferID] = Stocks::BufferMachines();
                }
                insert_machine(machine_id, stocks.buffer_machines[bufferID].ins);
              }
            }
            for (const auto &part: op->in) {
              if (part.storage) {
                uint storageID = json_factory.all_storages.find(part.storage->ID)->second.first;

                if (stocks.in_storages.find(machine_id) == stocks.in_storages.end()) {
                  stocks.in_storages[machine_id] = Stocks::Storages();
                }
                insert_storage(storage_in_zone(json_zone, part.storage->ID), storageID,
                               stocks.in_storages[machine_id]);
              } else if (part.buffer and result == InZoneResult::LOCAL_FOUND) { // buffer
                uint bufferID = json_factory.all_buffers.find(part.buffer->ID)->second.first;

                if (stocks.machine_buffers.find(machine_id) == stocks.machine_buffers.end()) {
                  stocks.machine_buffers[machine_id] = Stocks::MachineBuffers();
                }
                insert_buffer(json_zone, part.buffer->ID, bufferID, stocks.machine_buffers[machine_id].ins);
                if (stocks.buffer_machines.find(bufferID) == stocks.buffer_machines.end()) {
                  stocks.buffer_machines[bufferID] = Stocks::BufferMachines();
                }
                insert_machine(machine_id, stocks.buffer_machines[bufferID].outs);
              }
            }
          }
          op = op->next_operation;
        }
      }
    }
    return stocks;
  }

  StorageMachineExchanges get_storage_machine_exchanges(const json_dsl::Factory &json_factory) const {
    StorageMachineExchanges storages;

    for (const auto &product: json_factory.catalog) {
      auto op = product.second.second->program.first_operation;

      if (op) {
        while (op) {
          uint machine_id = json_factory.all_machines.find(op->machine_ID->ID)->second.first;
          uint zone_id = json_factory.all_zones.find(
            top_zone_of_machine(op->machine_ID->ID, json_factory)->ID)->second.index;

          for (const auto &part: op->out) {
            if (part.storage) {
              uint storageID = json_factory.all_storages.find(part.storage->ID)->second.first;

              if (storages.ins.find(storageID) == storages.ins.end()) {
                storages.ins[storageID] = StorageMachineExchanges::Machines();
              }
              if (
                std::find(storages.ins[storageID].second.begin(), storages.ins[storageID].second.end(),
                          machine_id) == storages.ins[storageID].second.end()) {
                storages.ins[storageID].first.push_back(zone_id);
                storages.ins[storageID].second.push_back(machine_id);
              }
            }
          }
          for (const auto &part: op->in) {
            if (part.storage) {
              uint storageID = json_factory.all_storages.find(part.storage->ID)->second.first;

              if (storages.outs.find(storageID) == storages.outs.end()) {
                storages.outs[storageID] = StorageMachineExchanges::Machines();
              }
              if (std::find(storages.outs[storageID].second.begin(),
                            storages.outs[storageID].second.end(),
                            machine_id) == storages.outs[storageID].second.end()) {
                storages.outs[storageID].first.push_back(zone_id);
                storages.outs[storageID].second.push_back(machine_id);
              }
            }
          }
          op = op->next_operation;
        }
      }
    }
    return storages;
  }

  Storages get_storages(const json_dsl::Factory &json_factory) const {
    Storages storages;

    for (const auto &s: json_factory.storages) {
      Storage storage;

      storage.id = s.second->ID;
      storage.index = json_factory.all_storages.find(s.second->ID)->second.first;
      storage.capacity = s.second->capacity;
      for (const auto &q: s.second->initial) {
        storage.initial_quantities[json_factory.all_items.find(q.item->id)->second.first] = q.quantity;
      }
      storages.push_back(storage);
    }
    return storages;
  }

  Storages get_storages(const json_dsl::Zone &json_zone, const json_dsl::Factory &json_factory) const {
    Storages storages;

    for (const auto &s: json_zone.storages) {
      Storage storage;

      storage.id = s.second->ID;
      storage.index = json_factory.all_storages.find(s.second->ID)->second.first;
      storage.capacity = s.second->capacity;
      for (const auto &q: s.second->initial) {
        storage.initial_quantities[json_factory.all_items.find(q.item->id)->second.first] = q.quantity;
      }
      storages.push_back(storage);
    }
    return storages;
  }

  Zones get_zones(const std::map<std::string, std::shared_ptr<json_dsl::Zone>> &json_zones,
                  const std::map<std::string, json_dsl::Factory::AllZone> &json_all_zones,
                  const json_dsl::Factory &json_factory) const {
    Zones zones;

    for (const auto &p: json_zones) {
      zones[json_all_zones.find(p.first)->second.index] =
        Zone{p.first, json_all_zones.find(p.first)->second.index,
             get_zones(p.second->sub_zones, json_all_zones, json_factory),
             get_machines(*p.second, json_factory),
             get_flows(*p.second, json_factory),
             get_stocks(*p.second, json_factory),
             get_storages(*p.second, json_factory),
             get_buffers(*p.second, json_factory),
             get_storage_machine_exchanges(json_factory),
             get_all_machines(*p.second, json_factory)};
    }
    return zones;
  }

  MachineProductOperationDurations get_durations(const json_dsl::Factory &json_factory) const {
    MachineProductOperationDurations durations;

    for (const auto &machine: json_factory.all_machines) {
      uint machine_id = machine.second.first;
      ProductOperationDurations product_durations;

      for (const auto &product: json_factory.catalog) {
        auto product_id = product.second.first;
        OperationDurations operation_durations;
        auto op = product.second.second->program.first_operation;
        OperationIndex operation_index = 0;

        if (op) {
          while (op) {
            if (json_factory.all_machines.find(op->machine_ID->ID)->second.first == machine_id) {
              operation_durations.insert(std::make_pair(operation_index,
                                                        ProcessorDurations(op->loading_time, op->setup_time,
                                                                           op->processing_time,
                                                                           op->unloading_time)));
            }
            op = op->next_operation;
            ++operation_index;
          }
        }
        if (not operation_durations.empty()) {
          product_durations[product_id] = operation_durations;
        }
      }
      durations[machine_id] = product_durations;
    }
    return durations;
  }

  void insert_buffer(const json_dsl::Zone &zone, const std::string &buffer_id, uint bufferID,
                     std::vector<uint> &buffers) const {
    // it's a buffer of zone?
    if (std::find_if(zone.buffers.begin(), zone.buffers.end(), [buffer_id](const auto &buffer) {
      return buffer.first == buffer_id;
    }) != zone.buffers.end()) {
      // the buffer is in list?
      if (std::find_if(buffers.begin(), buffers.end(), [bufferID](const auto &buffer) {
        return buffer == bufferID;
      }) == buffers.end()) {
        buffers.emplace_back(bufferID);
      }
    } else {
      assert(false);
    }
  }

  void insert_machine(uint machineID, std::vector<uint> &machines) const {
    // the machine is in list?
    if (std::find_if(machines.begin(), machines.end(), [machineID](const auto &machine) {
      return machine == machineID;
    }) == machines.end()) {
      machines.emplace_back(machineID);
    }
  }

  void insert_storage(uint storageID, Stocks::Storages &storages) const {
    if (std::find_if(storages.internals.begin(), storages.internals.end(), [storageID](const auto &storage) {
      return storage == storageID;
    }) == storages.internals.end()) {
      storages.internals.emplace_back(storageID);
    }
  }

  void insert_storage(const InZoneResult &result, uint storageID, Stocks::Storages &storages) const {
    if (result == InZoneResult::LOCAL_FOUND or result == InZoneResult::SUB_ZONE_FOUND) {
      if (std::find_if(storages.internals.begin(), storages.internals.end(), [storageID](const auto &storage) {
        return storage == storageID;
      }) == storages.internals.end()) {
        storages.internals.emplace_back(storageID);
      }
    } else {
      if (std::find_if(storages.externals.begin(), storages.externals.end(), [storageID](const auto &storage) {
        return storage == storageID;
      }) == storages.externals.end()) {
        storages.externals.emplace_back(storageID);
      }
    }
  }

  InZoneResult machine_in_zone(const json_dsl::Zone &top_zone, const std::string &machine_id) const {
    bool found = false;
    bool local = true;
    std::stack<const json_dsl::Zone *> zone_stack;

    zone_stack.push(&top_zone);
    while (not found and not zone_stack.empty()) {
      const json_dsl::Zone *z = zone_stack.top();

      zone_stack.pop();
      found = z->machines.find(machine_id) != z->machines.end();
      if (not found) {
        local = false;
        for (const auto &sub_zone: z->sub_zones) {
          zone_stack.push(sub_zone.second.get());
        }
      }
    }
    return found ? (local ? InZoneResult::LOCAL_FOUND : InZoneResult::SUB_ZONE_FOUND) : InZoneResult::NOT_FOUND;
  }

  InZoneResult storage_in_zone(const json_dsl::Zone &top_zone, const std::string &storage_id) const {
    bool found = false;
    bool local = true;
    std::stack<const json_dsl::Zone *> zone_stack;

    zone_stack.push(&top_zone);
    while (not found and not zone_stack.empty()) {
      const json_dsl::Zone *z = zone_stack.top();

      zone_stack.pop();
      found = z->storages.find(storage_id) != z->storages.end();
      if (not found) {
        local = false;
        for (const auto &sub_zone: z->sub_zones) {
          zone_stack.push(sub_zone.second.get());
        }
      }
    }
    return found ? (local ? InZoneResult::LOCAL_FOUND : InZoneResult::SUB_ZONE_FOUND) : InZoneResult::NOT_FOUND;
  }

  const json_dsl::Zone *zone_of_machine(const json_dsl::Zone &top_zone, const std::string &machine_id) const {
    std::stack<const json_dsl::Zone *> zone_stack;

    zone_stack.push(&top_zone);
    while (not zone_stack.empty()) {
      const json_dsl::Zone *z = zone_stack.top();
      bool found = z->machines.find(machine_id) != z->machines.end();

      zone_stack.pop();
      if (not found) {
        for (const auto &sub_zone: z->sub_zones) {
          zone_stack.push(sub_zone.second.get());
        }
      } else {
        return z;
      }
    }
    return nullptr;
  }

  const json_dsl::Zone *
  zone_of_machine(const std::string &machine_id, const json_dsl::Factory &json_factory) const {
    for (const auto &zone: json_factory.all_zones) {
      const json_dsl::Zone *z = zone_of_machine(*zone.second.zone, machine_id);

      if (z) { return z; }
    }
    return nullptr;
  }

  const json_dsl::Zone *
  top_zone_of_machine(const std::string &machine_id, const json_dsl::Factory &json_factory) const {
    for (const auto &zone: json_factory.all_zones) {
      const json_dsl::Zone *z = zone_of_machine(*zone.second.zone, machine_id);

      if (z) { return zone.second.zone.get(); }
    }
    return nullptr;
  }

  Factory operator()(const json_dsl::Factory &json_factory) const {
    return {get_storage_machine_exchanges(json_factory), build_programs(json_factory),
            get_zones(json_factory.zones, json_factory.all_zones, json_factory),
            get_durations(json_factory), get_storages(json_factory), get_machines(json_factory)};
  }

};

}

#endif //ARTIS_FACTORY_DSL_TO_DEVS_TRANSFORMER_HPP
