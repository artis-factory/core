/**
 * @file ZoneRouter.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ZoneRouter.hpp"

namespace artis::factory {

void ZoneRouter::dint(const Time & /* t */) {
  switch (_phase) {
    case Phase::INIT: {
      _phase = Phase::WAIT;
      break;
    }
    case Phase::WAIT: {
      break;
    }
    case Phase::SEND: {
      auto it = next_po();

      assert(it != _pending_po.cend());

      if (not (*it)->is_finish() and (*it)->current_operation().get_type() != CHANGE_ZONE_TYPE) {
        _available_machines[(*it)->current_operation().get_loc_index()]--;
      }
      _pending_po.erase(it);
      if (std::find_if(_pending_po.cbegin(), _pending_po.cend(), [this](const auto &e) {
        return e->is_finish() or e->current_operation().get_type() == CHANGE_ZONE_TYPE or
               _available_machines[e->current_operation().get_loc_index()] > 0;
      }) != _pending_po.cend()) {
        _phase = Phase::SEND;
      } else {
        _phase = Phase::WAIT;
      }
      break;
    }
  }
}

void ZoneRouter::dext(const Time &t, const Time & /* e */, const Bag &bag) {
  std::for_each(bag.begin(), bag.end(), [t, this](const common::event::ExternalEvent<common::DoubleTime> &event) {
                  if (event.port_index() == inputs::IN_PO) {
                    uint8_t *data = nullptr;

                    event.data()(data);
                    _pending_po.push_back(std::make_unique<ProductionOrder>(data, event.data().size()));
                    _pending_po.back()->next();

//#ifdef WITH_TRACE
//                    Trace::trace()
//                      << TraceElement(get_full_name(), t,
//                                      artis::common::FormalismType::PDEVS,
//                                      artis::common::FunctionType::DELTA_EXT,
//                                      artis::common::LevelType::USER)
//                      << "IN PO = " << _pending_po.back()->to_string();
//                    Trace::trace().flush();
//#endif

                  } else if (event.port_index() == inputs::FINISH) {
                    unsigned int index;

                    event.data()(index);
                    _available_machines[index]++;

//#ifdef WITH_TRACE
//                    Trace::trace()
//                      << TraceElement(get_full_name(), t,
//                                      artis::common::FormalismType::PDEVS,
//                                      artis::common::FunctionType::DELTA_EXT,
//                                      artis::common::LevelType::USER)
//                      << "FINISH = " << index;
//                    Trace::trace().flush();
//#endif

                  } else if (event.port_index() >= inputs::IN_M and event.port_index() < inputs::IN_SZ) {
                    uint8_t *data = nullptr;

                    event.data()(data);
                    _pending_po.push_back(std::make_unique<ProductionOrder>(data, event.data().size()));
                    _available_machines[event.port_index() - inputs::IN_M]++;
                    _pending_po.back()->next();

//#ifdef WITH_TRACE
//                    Trace::trace()
//                      << TraceElement(get_full_name(), t,
//                                      artis::common::FormalismType::PDEVS,
//                                      artis::common::FunctionType::DELTA_EXT,
//                                      artis::common::LevelType::USER)
//                      << "IN MACHINE = " <<  _pending_po.back()->to_string();
//                    Trace::trace().flush();
//#endif

                  } else if (event.port_index() >= inputs::IN_SZ) {
                      uint8_t *data = nullptr;

                      event.data()(data);
                      _pending_po.push_back(std::make_unique<ProductionOrder>(data, event.data().size()));
                      if (_pending_po.back()->current_operation().get_loc_index() == _parameters.index) {
                        _pending_po.back()->next();

//#ifdef WITH_TRACE
//                        Trace::trace()
//                          << TraceElement(get_full_name(), t,
//                                          artis::common::FormalismType::PDEVS,
//                                          artis::common::FunctionType::DELTA_EXT,
//                                          artis::common::LevelType::USER)
//                          << "IN SUB ZONE = " <<  _pending_po.back()->to_string();
//                        Trace::trace().flush();
//#endif

                      }
                  }
                }
  );
  if (std::find_if(_pending_po.cbegin(), _pending_po.cend(), [this](const auto &e) {
    return e->is_finish() or e->current_operation().get_type() == CHANGE_ZONE_TYPE or
           _available_machines[e->current_operation().get_loc_index()] > 0;
  }) != _pending_po.cend()) {
    _phase = Phase::SEND;
  } else {
    _phase = Phase::WAIT;
  }
}

void ZoneRouter::start(const Time & /* t */) {
  _phase = Phase::INIT;
  for (const auto &e: _parameters.machines) {
    _available_machines[e.index] = e.capacity;
  }
}

Time ZoneRouter::ta(const Time & /* t */) const {
  switch (_phase) {
    case Phase::INIT:
      return 0;
    case Phase::WAIT:
      return artis::common::DoubleTime::infinity;
    case Phase::SEND:
      return 0;
  }
  return artis::common::DoubleTime::infinity;
}

Bag ZoneRouter::lambda(const Time & /* t */) const {
  Bag bag;

  if (_phase == Phase::SEND) {
    if (not _pending_po.empty()) {
      auto it = next_po();

      assert(it != _pending_po.cend());

      auto po = it->get();

      if (po->is_finish()) {
        bag.push_back(ExternalEvent(outputs::OUT_PO, common::event::Value(po->_buffer, po->_size)));
      } else if (po->current_operation().get_type() == CHANGE_ZONE_TYPE) {
        auto it_zone = _parameters.sub_zones.find(po->current_operation().get_loc_index());

        if (it_zone == _parameters.sub_zones.cend()) {
          bag.push_back(ExternalEvent(outputs::OUT_PO, common::event::Value(po->_buffer, po->_size)));
        } else {
          bag.push_back(ExternalEvent(outputs::OUT_SZ + int(it_zone->first), common::event::Value(po->_buffer, po->_size)));
        }
      } else if (po->current_operation().get_type() == CHANGE_MACHINE_TYPE) {
        bag.push_back(ExternalEvent(outputs::OUT_M + po->current_operation().get_loc_index(),
                                    common::event::Value(po->_buffer, po->_size)));
      }
    }
  }
  return bag;
}

artis::common::event::Value ZoneRouter::observe(const Time & /* t */, unsigned int index) const {
  switch (index) {
    case vars::WAITING_PO_NUMBER:
      return (unsigned int) _pending_po.size();
  }
  return {};
}

ZoneRouter::ProductionOrders::const_iterator ZoneRouter::next_po() const {
  auto it = std::find_if(_pending_po.cbegin(), _pending_po.cend(), [](const auto &e) {
    return e->is_finish() or e->current_operation().get_type() == CHANGE_ZONE_TYPE;
  });

  if (it != _pending_po.cend()) {
    return it;
  } else {
    return std::find_if(_pending_po.cbegin(), _pending_po.cend(),
                        [this](const auto &e) { return _available_machines.at(e->current_operation().get_loc_index()) > 0; });
  }
}

} // namespace artis::factory