/**
 * @file Stock.tpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace artis::factory {

template < typename Model, typename Parameters>
void Stock<Model, Parameters>::dint(const Time & /* t */) {
  switch (_phase) {
    case Phase::SEND_AVAILABLE: {
      _available_demands.clear();
      _phase = Phase::WAIT;
      break;
    }
    case Phase::SEND: {

      _satisfied_item_demands.clear();
      _phase = Phase::WAIT;
      break;
    }
    case Phase::WAIT: {
      assert(false);
    }
  }
}

template < typename Model, typename Parameters>
void Stock<Model, Parameters>::dext(const Time &t, const Time & /* e */, const Bag &bag) {
  std::for_each(bag.begin(), bag.end(), [t, this](const ExternalEvent &event) {
    if (event.port_index() >= inputs::IN_ITEM and event.port_index() < inputs::IN_ITEM_AVAILABLE) {
      unsigned int machine_id = event.port_index() - inputs::IN_ITEM;
      ItemInput entry;

      event.data()(entry);
      if (_parameters._capacity == -1 or (int) _entries.size() < _parameters._capacity) {
        _entries.emplace_back(entry.item_id, entry.quantity, entry.po_data->po_id, entry.po_data->po_index, t);
      } else {
        assert(false);
      }

#ifdef WITH_TRACE
      Trace::trace()
        << TraceElement(Stock<Model, Parameters>::get_full_name(), t,
                        artis::common::FormalismType::PDEVS,
                        artis::common::FunctionType::DELTA_EXT,
                        artis::common::LevelType::USER)
        << "IN ITEM item = " << entry.to_string();
      Trace::trace().flush();
#endif

    } else if (event.port_index() >= inputs::IN_ITEM_AVAILABLE and event.port_index() < inputs::IN_SPACE_AVAILABLE) {
      unsigned int machine_id = event.port_index() - inputs::IN_ITEM_AVAILABLE;
      ItemDemand demand;

      event.data()(demand);

      auto it = std::find_if(_entries.begin(), _entries.end(), [demand](const auto &e) {
        if (e.po_data) {
          return e.po_data->po_id == demand.po_data->po_id and /*e.po_data->po_index == demand.po_data->po_index and*/
                 e.item_id == demand.item_id and e.quantity >= demand.quantity;
        } else {
          return e.item_id == demand.item_id and e.quantity >= demand.quantity;
        }
      });

      if (it != _entries.end()) {
        if (it->quantity == demand.quantity) {
          _entries.erase(it);
        } else if (it->quantity > demand.quantity) {
          it->quantity -= demand.quantity;
        } else {
          _unsatisfied_item_demands.emplace_back(machine_id, demand);
        }
        _satisfied_item_demands.emplace_back(machine_id, demand);
        _phase = Phase::SEND;
      } else {
        _unsatisfied_item_demands.emplace_back(machine_id, demand);
      }

#ifdef WITH_TRACE
      Trace::trace()
        << TraceElement(Stock<Model, Parameters>::get_full_name(), t,
                        artis::common::FormalismType::PDEVS,
                        artis::common::FunctionType::DELTA_EXT,
                        artis::common::LevelType::USER)
        << "ITEM DEMAND item = " << demand.to_string();
      Trace::trace().flush();
#endif

    } else if (event.port_index() >= inputs::IN_SPACE_AVAILABLE) {
      unsigned int machine_id = event.port_index() - inputs::IN_SPACE_AVAILABLE;
      unsigned int po_id;

      event.data()(po_id);
      _available_demands.push_back(AvailableDemand{machine_id, po_id, false});
      if (_parameters._capacity == -1 or (int) _entries.size() < _parameters._capacity) {
        auto it = std::find_if(_waiting_machine_ids.begin(), _waiting_machine_ids.end(),
                               [machine_id, po_id](const auto &e) {
                                 return machine_id == e.first and po_id == e.second;
                               });

#ifdef WITH_TRACE
        Trace::trace()
          << TraceElement(Stock<Model, Parameters>::get_full_name(), t,
                          artis::common::FormalismType::PDEVS,
                          artis::common::FunctionType::DELTA_EXT,
                          artis::common::LevelType::USER)
          << "SPACE AVAILABLE item = " << machine_id << " " << po_id << " " << _entries.size() << " " << _parameters._capacity;
        Trace::trace().flush();
#endif

        if (it != _waiting_machine_ids.end()) {
          _waiting_machine_ids.erase(it);
        }
        _available_demands.back()._available = true;
        _phase = Phase::SEND_AVAILABLE;
      } else {
        _waiting_machine_ids.emplace_back(machine_id, po_id);
        _phase = Phase::SEND_AVAILABLE;
      }
    }
  });
}

template < typename Model, typename Parameters>
void Stock<Model, Parameters>::start(const Time & /* t */) {
  _phase = Phase::WAIT;
}

template < typename Model, typename Parameters>
Time Stock<Model, Parameters>::ta(const Time & /* t */) const {
  switch (_phase) {
    case Phase::WAIT:
      return artis::common::DoubleTime::infinity;
    case Phase::SEND:
    case Phase::SEND_AVAILABLE:
      return 0;
  }
  return artis::common::DoubleTime::infinity;
}

template < typename Model, typename Parameters>
Bag Stock<Model, Parameters>::lambda(const Time & t) const {
  Bag bag;

  if (_phase == Phase::SEND) {
    for (const auto &e: _waiting_machine_ids) {
      bag.push_back(ExternalEvent(outputs::OUT_SPACE_AVAILABLE + e.first,
                                  StockAvailableResponse(e.second, StockAvailableResponse::NEW_AVAILABLE)));

#ifdef WITH_TRACE
      Trace::trace()
        << TraceElement(Stock<Model, Parameters>::get_full_name(), t,
                        artis::common::FormalismType::PDEVS,
                        artis::common::FunctionType::LAMBDA,
                        artis::common::LevelType::USER)
        << "OUT_SPACE_AVAILABLE = " << e.second;
      Trace::trace().flush();
#endif

    }
    for (const auto &e: _satisfied_item_demands) {
      bag.push_back(ExternalEvent(outputs::OUT_ITEM + e.first, ProductResponse(e.second.item_id, e.second.po_data->po_id)));

#ifdef WITH_TRACE
      Trace::trace()
        << TraceElement(Stock<Model, Parameters>::get_full_name(), t,
                        artis::common::FormalismType::PDEVS,
                        artis::common::FunctionType::LAMBDA,
                        artis::common::LevelType::USER)
        << "OUT_ITEM = " << e.second.item_id << " with quantity " << e.second.quantity << " for po " << e.second.po_data->po_id << " to machine " << e.first;
      Trace::trace().flush();
#endif


    }
  } else if (_phase == Phase::SEND_AVAILABLE) {
    for (const auto &demand: _available_demands) {
      bag.push_back(ExternalEvent(outputs::OUT_SPACE_AVAILABLE + demand._machine_id,
                                  StockAvailableResponse(demand._po_id,
                                                         demand._available ? StockAvailableResponse::AVAILABLE
                                                                           : StockAvailableResponse::NOT_AVAILABLE)));

#ifdef WITH_TRACE
      Trace::trace()
        << TraceElement(Stock<Model, Parameters>::get_full_name(), t,
                        artis::common::FormalismType::PDEVS,
                        artis::common::FunctionType::LAMBDA,
                        artis::common::LevelType::USER)
        << "OUT_SPACE_AVAILABLE = " << demand._po_id << " to machine " << demand._machine_id << " OK " << demand._available;
      Trace::trace().flush();
#endif

    }
  }
  return bag;
}

template < typename Model, typename Parameters>
artis::common::event::Value Stock<Model, Parameters>::observe(const Time & /* t */, unsigned int index) const {
  switch (index) {
    case vars::ENTRY_NUMBER:
      return (unsigned int) _entries.size();
  }
  return {};
}

} // namespace artis::factory