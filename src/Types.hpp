/**
 * @file Types.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_FACTORY_TYPES_HPP
#define ARTIS_FACTORY_TYPES_HPP

namespace artis::factory {

// zone type
const uint8_t FLOW_SHOP = 0x00;
const uint8_t JOB_SHOP = 0x01;
const uint8_t OPEN_SHOP = 0x02;

// machine type
const uint8_t PROCESSOR = 0x00;
const uint8_t SINK = 0x01;
const uint8_t SEPARATOR = 0x02;
const uint8_t COMBINER = 0x03;
const uint8_t CONVEYOR = 0x04;

// operation type
const uint8_t CHANGE_ZONE_TYPE = 0x00;
const uint8_t CHANGE_MACHINE_TYPE = 0x01;

}

#endif //ARTIS_FACTORY_TYPES_HPP
