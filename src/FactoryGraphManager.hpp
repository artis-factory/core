/**
 * @file FactoryGraphManager.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_FACTORY_FACTORY_GRAPH_MANAGER_HPP
#define ARTIS_FACTORY_FACTORY_GRAPH_MANAGER_HPP

#include "Base.hpp"
#include "Buffer.hpp"
#include "dsl_to_devs/Model.hpp"
#include "OrderArrival.hpp"
#include "Processor.hpp"
#include "Router.hpp"
#include "Sink.hpp"
#include "Storage.hpp"
#include "ZoneGraphManager.hpp"
#include "ZoneRouter.hpp"

#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <memory>

namespace artis::factory {

struct FactoryGraphManagerParameters {
  const dsl_to_devs::Factory &factory;
};

class FactoryGraphManager
  : public artis::pdevs::GraphManager<artis::common::DoubleTime, artis::common::NoParameters, FactoryGraphManagerParameters> {
public:
  enum sub_models {
    ORDER_ARRIVAL, ROUTER, STORAGE = 1000, ZONE = 2000
  };

  FactoryGraphManager(Coordinator *coordinator, const artis::common::NoParameters &parameters,
                      const FactoryGraphManagerParameters &graph_parameters) :
    artis::pdevs::GraphManager<artis::common::DoubleTime, artis::common::NoParameters, FactoryGraphManagerParameters>(
      coordinator, parameters, graph_parameters) {
    // order arrival => OK
    {
      OrderArrivalParameters generator_parameters{graph_parameters.factory.programs, 8736, 60, 120, 10, {},
                                                  {0, 0}, {50, 50}};

      _order_arrival = std::make_unique<OrderArrivalSimulator>("G", generator_parameters);
      this->add_child(sub_models::ORDER_ARRIVAL, _order_arrival.get());
    }

    // router => OK
    {
      _router = std::make_unique<RouterSimulator>("R", RouterParameters(graph_parameters.factory.zones));
      this->add_child(ROUTER, _router.get());
      out({_order_arrival.get(), artis::factory::OrderArrival::outputs::OUT})
        >> in({_router.get(), artis::factory::Router::inputs::IN});
    }

    // zones => OK
    {
      for (const auto &zone: graph_parameters.factory.zones) {
        ZoneGraphManagerParameters zone_parameters{zone.second.id, zone.first, zone.second.sub_zones,
                                                   zone.second.machines, zone.second.in_outs,
                                                   zone.second.stocks, zone.second.storages, zone.second.buffers,
                                                   graph_parameters.factory.durations,
                                                   graph_parameters.factory.storage_machine_exchanges,
                                                   zone.second.all_machines};

        _zones[zone.first] = std::make_unique<ZoneCoordinator>(zone.second.id, common::NoParameters(), zone_parameters);
        this->add_child(ZONE + zone.first, _zones[zone.first].get());
        out({_router.get(), Router::outputs::OUT_ZONE + zone.first})
          >> in({_zones[zone.first].get(), ZoneGraphManager::inputs::IN_PO});
        out({_zones[zone.first].get(), ZoneGraphManager::outputs::OUT_PO})
          >> out({_router.get(), Router::inputs::IN_ZONE + zone.first});
      }
    }

    // storages => OK
    {
      for (const auto &storage: graph_parameters.factory.storages) {
        StorageParameters storage_parameters{
          {graph_parameters.factory.storage_machine_exchanges.ins.find(storage.index) !=
           graph_parameters.factory.storage_machine_exchanges.ins.end()
           ? graph_parameters.factory.storage_machine_exchanges.ins.find(storage.index)->second.second
           : std::vector<unsigned int>(),
           graph_parameters.factory.storage_machine_exchanges.outs.find(storage.index) !=
           graph_parameters.factory.storage_machine_exchanges.outs.end()
           ? graph_parameters.factory.storage_machine_exchanges.outs.find(storage.index)->second.second
           : std::vector<unsigned int>(),
           storage.capacity}, storage.initial_quantities};

        _storages[storage.index] = std::make_unique<StorageSimulator>("S_" + storage.id, storage_parameters);
        this->add_child(sub_models::STORAGE + storage.index, _storages[storage.index].get());
      }
    }

    // connection zones <-> storages => OK
    {
      for (const auto &storage: graph_parameters.factory.storages) {
        for (const auto &m: graph_parameters.factory.machines) {
          if (graph_parameters.factory.storage_machine_exchanges.outs.find(storage.index) !=
              graph_parameters.factory.storage_machine_exchanges.outs.end()) {
            auto it_out = std::find(
              graph_parameters.factory.storage_machine_exchanges.outs.find(storage.index)->second.second.begin(),
              graph_parameters.factory.storage_machine_exchanges.outs.find(storage.index)->second.second.end(),
              m.index);

            // out of storage
            if (it_out !=
                graph_parameters.factory.storage_machine_exchanges.outs.find(storage.index)->second.second.end()) {
              auto zone_index = graph_parameters.factory.storage_machine_exchanges.outs.find(
                storage.index)->second.first[
                it_out -
                graph_parameters.factory.storage_machine_exchanges.outs.find(storage.index)->second.second.begin()];
              uint port_index = ZoneGraphManager::inputs::IN_ITEM + 100 * m.index + storage.index;

              if (not _zones[zone_index]->exist_in_port(port_index)) {
                _zones[zone_index]->add_in_port({port_index, "IN_ITEM_" + m.id + "_" + storage.id});
                _zones[zone_index]->add_in_port(
                  {ZoneGraphManager::inputs::IN_ITEM_AVAILABLE + 100 * m.index + storage.index,
                   "IN_ITEM_AVAILABLE" + m.id + "_" + storage.id});
                _zones[zone_index]->add_out_port(
                  {ZoneGraphManager::outputs::OUT_ITEM_AVAILABLE + 100 * m.index + storage.index,
                   "OUT_ITEM_AVAILABLE" + m.id + "_" + storage.id});
              }
              if (not exist_link(_storages[storage.index].get(), Storage::outputs::OUT_ITEM + m.index,
                                 common::InOutType::OUT,
                                 _zones[zone_index].get(), port_index, common::InOutType::IN)) {
                out({_storages[storage.index].get(), Storage::outputs::OUT_ITEM + m.index})
                  >> in({_zones[zone_index].get(), port_index});
                out({_storages[storage.index].get(), Storage::outputs::OUT_ITEM_AVAILABLE + m.index})
                  >> in({_zones[zone_index].get(),
                         ZoneGraphManager::inputs::IN_ITEM_AVAILABLE + 100 * m.index + storage.index});
                out({_zones[zone_index].get(),
                     ZoneGraphManager::outputs::OUT_ITEM_AVAILABLE + 100 * m.index + storage.index})
                  >> in({_storages[storage.index].get(), Storage::inputs::IN_ITEM_AVAILABLE + m.index});
              }
            }
          }
          // in of storage
          if (graph_parameters.factory.storage_machine_exchanges.ins.find(storage.index) !=
              graph_parameters.factory.storage_machine_exchanges.ins.end()) {
            auto it_in = std::find(
              graph_parameters.factory.storage_machine_exchanges.ins.find(storage.index)->second.second.begin(),
              graph_parameters.factory.storage_machine_exchanges.ins.find(storage.index)->second.second.end(),
              m.index);

            if (
              std::find(
                graph_parameters.factory.storage_machine_exchanges.ins.find(storage.index)->second.second.begin(),
                graph_parameters.factory.storage_machine_exchanges.ins.find(storage.index)->second.second.end(),
                m.index) !=
              graph_parameters.factory.storage_machine_exchanges.ins.find(storage.index)->second.second.end()) {
              auto zone_index = graph_parameters.factory.storage_machine_exchanges.ins.find(
                storage.index)->second.first[
                it_in -
                graph_parameters.factory.storage_machine_exchanges.ins.find(storage.index)->second.second.begin()];
              uint port_index = ZoneGraphManager::outputs::OUT_ITEM + 100 * m.index + storage.index;

              if (not _zones[zone_index]->exist_out_port(port_index)) {
                _zones[zone_index]->add_out_port({port_index, "OUT_ITEM_" + m.id + "_" + storage.id});
                _zones[zone_index]->add_in_port(
                  {ZoneGraphManager::inputs::IN_SPACE_AVAILABLE + 100 * m.index + storage.index,
                   "IN_SPACE_AVAILABLE" + m.id + "_" + storage.id});
                _zones[zone_index]->add_out_port(
                  {ZoneGraphManager::outputs::OUT_SPACE_AVAILABLE + 100 * m.index + storage.index,
                   "OUT_SPACE_AVAILABLE" + m.id + "_" + storage.id});
              }
              if (not exist_link(_storages[storage.index].get(), Storage::outputs::OUT_ITEM + m.index,
                                 common::InOutType::OUT,
                                 _zones[zone_index].get(), port_index, common::InOutType::IN)) {
                out({_zones[zone_index].get(), port_index})
                  >> in({_storages[storage.index].get(), Storage::inputs::IN_ITEM + m.index});
                out({_storages[storage.index].get(), Storage::outputs::OUT_SPACE_AVAILABLE + m.index})
                  >> in({_zones[zone_index].get(),
                         ZoneGraphManager::inputs::IN_SPACE_AVAILABLE + 100 * m.index + storage.index});
                out({_zones[zone_index].get(),
                     ZoneGraphManager::outputs::OUT_SPACE_AVAILABLE + 100 * m.index + storage.index})
                  >> in({_storages[storage.index].get(), Storage::inputs::IN_SPACE_AVAILABLE + m.index});
              }
            }
          }
        }
      }
    }
  }

private:
  using OrderArrivalSimulator = artis::pdevs::Simulator<artis::common::DoubleTime, artis::factory::OrderArrival, artis::factory::OrderArrivalParameters>;
  using RouterSimulator = artis::pdevs::Simulator<artis::common::DoubleTime, artis::factory::Router, artis::factory::RouterParameters>;
  using StorageSimulator = artis::pdevs::Simulator<artis::common::DoubleTime, artis::factory::Storage, artis::factory::StorageParameters>;
  using ZoneCoordinator = artis::pdevs::Coordinator<artis::common::DoubleTime, artis::factory::ZoneGraphManager, common::NoParameters, artis::factory::ZoneGraphManagerParameters>;

  std::unique_ptr<OrderArrivalSimulator> _order_arrival;
  std::unique_ptr<RouterSimulator> _router;
  std::map<uint, std::unique_ptr<StorageSimulator>> _storages;
  std::map<uint, std::unique_ptr<ZoneCoordinator>> _zones;
};

}

#endif //ARTIS_FACTORY_FACTORY_GRAPH_MANAGER_HPP
