/**
 * @file Separator.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_FACTORY_SEPARATOR_HPP
#define ARTIS_FACTORY_SEPARATOR_HPP

#include "Base.hpp"
#include "Machine.hpp"
#include "ProductionOrder.hpp"

namespace artis::factory {

struct SeparatorParameters : MachineParameters {
};

class Separator : public Dynamics<Separator, SeparatorParameters> {
public:
  struct inputs {
    enum values {
      IN
    };
  };

  struct outputs {
    enum values {
      OUT
    };
  };

  struct vars {
    enum values {
    };
  };

  Separator(const std::string &name, const Context<Separator, SeparatorParameters> &context)
    : Dynamics<Separator, SeparatorParameters>(name, context) {
    input_port({inputs::IN, "in"});
    output_port({outputs::OUT, "out"});
  }

  ~Separator() override = default;

  void dint(const Time &t) override;

  void dext(const Time &t, const Time &e, const Bag &bag) override;

  void start(const Time &t) override;

  Time ta(const Time &t) const override;

  Bag lambda(const Time &t) const override;

  artis::common::event::Value observe(const Time &t, unsigned int index) const override;

private:
  // TODO (state)
};

} // namespace artis::factory

#endif