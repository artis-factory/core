/**
 * @file Processor.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_FACTORY_PROCESSOR_HPP
#define ARTIS_FACTORY_PROCESSOR_HPP

#include <utility>

#include "Base.hpp"
#include "Machine.hpp"
#include "dsl_to_devs/Model.hpp"
#include "ProductionOrder.hpp"

namespace artis::factory {

struct ProcessorParameters : MachineParameters {
  ProcessorParameters(uint machine_id, uint machine_type, uint zone_id, uint capacity, uint loading_time,
                      uint setup_time, uint processing_time, uint unloading_time,
                      dsl_to_devs::Stocks stocks,
                      dsl_to_devs::ProductInOuts product_in_outs,
                      dsl_to_devs::MachineProductOperationDurations product_operation_durations) :
    MachineParameters(machine_id, machine_type, zone_id, capacity),
    durations(loading_time, setup_time, processing_time, unloading_time),
    stocks(std::move(stocks)),
    product_in_outs(std::move(product_in_outs)),
    product_operation_durations(std::move(product_operation_durations)) {}

  dsl_to_devs::ProcessorDurations durations;
  dsl_to_devs::Stocks stocks;
  dsl_to_devs::ProductInOuts product_in_outs;
  dsl_to_devs::MachineProductOperationDurations product_operation_durations;
};

class Processor : public Dynamics<Processor, ProcessorParameters> {
public:
  struct inputs {
    enum values {
      IN_PO, IN_ITEM = 1000, IN_ITEM_AVAILABLE = 2000, IN_SPACE_AVAILABLE = 3000
    };
  };

  struct outputs {
    enum values {
      OUT_PO, OUT_ITEM = 1000, OUT_ITEM_AVAILABLE = 2000, OUT_SPACE_AVAILABLE = 3000
    };
  };

  struct vars {
    enum values {
    };
  };

  Processor(const std::string &name, const Context<Processor, ProcessorParameters> &context)
    : Dynamics<Processor, ProcessorParameters>(name, context), _parameters(context.parameters()) {
    input_port({inputs::IN_PO, "in_po"});
    output_port({outputs::OUT_PO, "out_po"});

    // storage ports
    {
      for (const auto &storage: _parameters.stocks.out_storages[_parameters.machine_id].externals) {
        this->output_port(
          {outputs::OUT_ITEM + storage, "out_item_" + std::to_string(storage)});
        this->output_port(
          {outputs::OUT_SPACE_AVAILABLE + storage, "out_space_available_" + std::to_string(storage)});
        this->input_port(
          {inputs::IN_SPACE_AVAILABLE + storage, "in_space_available_" + std::to_string(storage)});
      }
      for (const auto &storage: _parameters.stocks.in_storages[_parameters.machine_id].externals) {
        this->input_port(
          {inputs::IN_ITEM + storage, "in_item_" + std::to_string(storage)});
        this->output_port(
          {outputs::OUT_ITEM_AVAILABLE + storage, "out_item_available_" + std::to_string(storage)});
        this->input_port(
          {inputs::IN_ITEM_AVAILABLE + storage, "in_item_available_" + std::to_string(storage)});
      }
      for (const auto &storage: _parameters.stocks.out_storages[_parameters.machine_id].internals) {
        this->output_port(
          {outputs::OUT_ITEM + storage, "out_item_" + std::to_string(storage)});
        this->output_port(
          {outputs::OUT_SPACE_AVAILABLE + storage, "out_space_available_" + std::to_string(storage)});
        this->input_port(
          {inputs::IN_SPACE_AVAILABLE + storage, "in_space_available_" + std::to_string(storage)});
      }
      for (const auto &storage: _parameters.stocks.in_storages[_parameters.machine_id].internals) {
        this->input_port(
          {inputs::IN_ITEM + storage, "in_item_" + std::to_string(storage)});
        this->output_port(
          {outputs::OUT_ITEM_AVAILABLE + storage, "out_item_available_" + std::to_string(storage)});
        this->input_port(
          {inputs::IN_ITEM_AVAILABLE + storage, "in_item_available_" + std::to_string(storage)});
      }
    }

    // buffer ports
    {
      const auto &in_buffers = _parameters.stocks.machine_buffers[_parameters.machine_id].ins;
      const auto &out_buffers = _parameters.stocks.machine_buffers[_parameters.machine_id].outs;

      for (const auto &buffer: out_buffers) {
        this->output_port(
          {outputs::OUT_ITEM + buffer, "out_item_" + std::to_string(buffer)});
        this->output_port(
          {outputs::OUT_SPACE_AVAILABLE + buffer, "out_space_available_" + std::to_string(buffer)});
        this->input_port(
          {inputs::IN_SPACE_AVAILABLE + buffer, "in_space_available_" + std::to_string(buffer)});
      }
      for (const auto &buffer: in_buffers) {
        this->input_port(
          {inputs::IN_ITEM + buffer, "in_item_" + std::to_string(buffer)});
        this->output_port(
          {outputs::OUT_ITEM_AVAILABLE + buffer, "out_item_available_" + std::to_string(buffer)});
        this->input_port(
          {inputs::IN_ITEM_AVAILABLE + buffer, "in_item_available_" + std::to_string(buffer)});
      }
    }
  }

  ~Processor() override = default;

  void dint(const Time &t) override;

  void dext(const Time &t, const Time &e, const Bag &bag) override;

  void start(const Time &t) override;

  Time ta(const Time &t) const override;

  Bag lambda(const Time &t) const override;

  artis::common::event::Value observe(const Time &t, unsigned int index) const override;

private:
  const dsl_to_devs::ProcessorDurations &durations(unsigned int product_id, std::size_t operation_index) const {
    if (_parameters.product_operation_durations.find(_parameters.machine_id)->second.find(product_id) ==
        _parameters.product_operation_durations.find(_parameters.machine_id)->second.cend()) {
      return _parameters.durations;
    } else {
      return _parameters.product_operation_durations.find(_parameters.machine_id)->second.find(product_id)->second.find(
        operation_index)->second;
    }
  }

  struct Phase {
    enum values {
      READY,
      WAIT,
      LOADING,
      PROCESSING,
      UNLOADING,
      ITEM_DEMAND,
      WAIT_SUPPLY,
      SEND_STOCK_AVAILABLE,
      WAIT_STOCK_AVAILABLE
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case READY:
          return "READY";
        case WAIT:
          return "WAIT";
        case LOADING:
          return "LOADING";
        case PROCESSING:
          return "PROCESSING";
        case UNLOADING:
          return "UNLOADING";
        case ITEM_DEMAND:
          return "DEMAND SUPPLY";
        case WAIT_SUPPLY:
          return "WAIT SUPPLY";
        case SEND_STOCK_AVAILABLE:
          return "SEND STOCK AVAILABLE";
        case WAIT_STOCK_AVAILABLE:
          return "WAIT STOCK AVAILABLE";
      }
      return "";
    }
  };

  struct Job {
    Phase::values _phase;
    std::unique_ptr<ProductionOrder> _po;
    unsigned int _demand_number;
    Time _next_time;
  };

// parameters
  ProcessorParameters _parameters;

// state
  std::map<unsigned int, Job> _jobs; // key = po_id
};

} // namespace artis::factory

#endif