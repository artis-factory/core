/**
 * @file ProductionOrder.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_FACTORY_PRODUCTION_ORDER_HPP
#define ARTIS_FACTORY_PRODUCTION_ORDER_HPP

#include <cstdint>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "Types.hpp"

namespace artis::factory {

// HEADER = [bit number (16 bits)][ID (16 bits)][parent ID (16 bits)][Due date (32 bits)][Product ID (16 bits)][index (8 bits)][operation number (8 bits)]
const std::size_t ZONE_SIZE = 7;     // bit number
const std::size_t MACHINE_SIZE = 4;  // bit number
const std::size_t TYPE_SIZE = 2;     // bit number
const std::size_t HEADER_SIZE = 15;  // byte number

const std::size_t BIT_NUMBER_POSITION = 0;
const std::size_t ID_POSITION = BIT_NUMBER_POSITION + 2;
const std::size_t PARENT_ID_POSITION = ID_POSITION + 2;
const std::size_t DUE_DATE_POSITION = PARENT_ID_POSITION + 2;
const std::size_t PRODUCT_ID_POSITION = DUE_DATE_POSITION + 4;
const std::size_t INDEX_POSITION = PRODUCT_ID_POSITION + 2;
const std::size_t OPERATION_INDEX_POSITION = INDEX_POSITION + 1;
const std::size_t OPERATION_NUMBER_POSITION = OPERATION_INDEX_POSITION + 1;

struct Operation {
  virtual uint8_t get_type() const = 0;

  virtual uint8_t get_loc_index() const = 0;

  virtual std::string to_string() const = 0;

  virtual ~Operation() = default;
};

struct ChangeZone : Operation {
  uint8_t _zone_id;

  explicit ChangeZone(uint8_t zone_id) : _zone_id(zone_id) {}

  explicit ChangeZone() { _zone_id = 0; }

  uint8_t get_type() const { return CHANGE_ZONE_TYPE; }

  uint8_t get_loc_index() const { return _zone_id; }

  std::string to_string() const {
    return "ChangeZone(" + std::to_string(_zone_id) + ")";
  }
};

struct ChangeMachine : Operation {
  uint8_t _machine_id;

  explicit ChangeMachine(uint8_t machine_id) : _machine_id(machine_id) {}

  explicit ChangeMachine() { _machine_id = 0; }

  uint8_t get_type() const { return CHANGE_MACHINE_TYPE; }

  uint8_t get_loc_index() const { return _machine_id; }

  std::string to_string() const {
    return "ChangeMachine(" + std::to_string((int) _machine_id) + ")";
  }
};

struct ZoneMachineSequence {
  ChangeZone _zone;
  std::vector<ChangeMachine> _machines;

  ZoneMachineSequence() = default;

  ZoneMachineSequence(uint8_t zone_index, const std::vector<uint8_t> &machines_indexes) {
    _zone._zone_id = zone_index;
    for (auto machinesIndex: machines_indexes) {
      _machines.emplace_back(machinesIndex);
    }
  }
};

using Program = std::vector<ZoneMachineSequence>;

using Programs = std::map<uint, Program>;

struct ProductionOrder {
  uint8_t *_buffer;
  std::size_t _size;
  std::size_t _bsize;
  std::unique_ptr<Operation> _current_operation;
  bool _is_finish;

  struct Reference {
    uint8_t _byte_index;
    uint8_t _bit_index;

    void operator+=(uint8_t p);
  };

  ProductionOrder() : _buffer(nullptr), _size(0), _bsize(0), _current_operation(nullptr), _is_finish(true) {}

  ProductionOrder(uint8_t *buffer, std::size_t size);

  ~ProductionOrder() { delete[] _buffer; }

  ProductionOrder(uint16_t ID, uint16_t parent_ID, uint32_t due_date, uint16_t product_ID, const Program &program);

  const Operation &current_operation() const { return *_current_operation; }

  uint16_t getID() const {
    return (_buffer[ID_POSITION] << 8) + _buffer[ID_POSITION + 1];
  }

  uint32_t get_due_date() const {
    return (_buffer[DUE_DATE_POSITION] << 24) + (_buffer[DUE_DATE_POSITION + 1] << 16) +
           (_buffer[DUE_DATE_POSITION + 2] << 8) + _buffer[DUE_DATE_POSITION + 3];
  }

  std::unique_ptr<Operation> get_operation(uint8_t index) const;

  uint16_t get_parentID() const {
    return (_buffer[PARENT_ID_POSITION] << 8) + _buffer[PARENT_ID_POSITION + 1];
  }

  uint16_t get_productID() const {
    return (_buffer[PRODUCT_ID_POSITION] << 8) + _buffer[PRODUCT_ID_POSITION + 1];
  }

  bool is_finish() const { return _is_finish; }

  std::unique_ptr<Operation> last_operation() const;

  void next();

  std::size_t operation_index() const { return _buffer[OPERATION_INDEX_POSITION]; }

  std::size_t operation_number() const {
    return _buffer[OPERATION_NUMBER_POSITION];
  }

  std::unique_ptr<Operation> previous_operation() const;

  std::string to_string() const;

  void update_operation(uint8_t index);
};

} // namespace artis::factory

#endif