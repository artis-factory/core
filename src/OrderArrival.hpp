/**
 * @file OrderArrival.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_FACTORY_PRODUCTION_ORDER_GENERATOR_HPP
#define ARTIS_FACTORY_PRODUCTION_ORDER_GENERATOR_HPP

#include "Base.hpp"
#include "ProductionOrder.hpp"

#include <deque>
#include <map>
#include <random>
#include <vector>

namespace artis::factory {

struct OrderArrivalParameters {
  std::map<unsigned int, Program> programs;
  uint random_seed;
  uint min_send_speed_rate;
  uint max_send_speed_rate;
  int po_number{-1};
  std::vector<unsigned int> po_order{};
  std::vector<unsigned int> product_processing_durations{};
  std::vector<double> product_proportions{};
  unsigned int due_date_window{};
};

class OrderArrival
  : public Dynamics<OrderArrival, OrderArrivalParameters> {
public:
  struct outputs {
    enum values {
      OUT
    };
  };

  struct vars {
    enum values {
      TOTAL_PO_NUMBER
    };
  };

  OrderArrival(const std::string &name,
                           const Context<OrderArrival, OrderArrivalParameters> &context)
    : Dynamics<OrderArrival, OrderArrivalParameters>(
    name, context),
      _parameters(context.parameters()),
      _rng(_parameters.random_seed > 0 ? std::mt19937(_parameters.random_seed) : std::mt19937(_rd())),
      _distrib_product(std::uniform_int_distribution<>(0, (int) _parameters.programs.size() - 1)),
      _distrib_send_speed(_parameters.min_send_speed_rate, _parameters.max_send_speed_rate) {
    for (auto const &program: _parameters.programs) {
      _productIDs.push_back(program.first);
    }
    output_port({outputs::OUT, "out"});
    observables({{vars::TOTAL_PO_NUMBER, "total_po_number"}});
  }

  ~OrderArrival() override = default;

  void dint(const Time & /* t */) override;

  void dext(const Time & /* t */, const Time & /* e */, const Bag & /* bag*/) override;

  void start(const Time & /* t */) override;

  Time ta(const Time & /* t */) const override;

  Bag lambda(const Time & /* t */) const override;

  artis::common::event::Value observe(const Time &t, unsigned int index) const override;

private:
  void generate_production_orders(const Time & t);

  struct Phase {
    enum values {
      INIT, SEND, FINISH
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case INIT:
          return "INIT";
        case SEND:
          return "SEND";
        case FINISH:
          return "FINISH";
      }
      return "";
    }
  };

  // parameters
  OrderArrivalParameters _parameters;

  // state
  Phase::values _phase;
  std::random_device _rd;
  std::mt19937 _rng;
  std::uniform_int_distribution<> _distrib_product;
  std::uniform_int_distribution<> _distrib_program;
  std::uniform_int_distribution<> _distrib_send_speed;

  std::vector<unsigned int> _productIDs;

  unsigned int _productID;
  std::vector<std::unique_ptr<ProductionOrder>> _pos;

  unsigned int _current_id;
  std::size_t _po_index;
  unsigned int _sigma;
};

} // namespace artis::factory

#endif