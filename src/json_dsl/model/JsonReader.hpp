/**
 * @file JsonReader.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_FACTORY_JSON_DSL_JSON_READER_HPP
#define ARTIS_FACTORY_JSON_DSL_JSON_READER_HPP

#include "Model.hpp"

#include <nlohmann/json.hpp>

namespace artis::factory::json_dsl {

class JsonReader {
public:
  JsonReader() = default;

  const Factory &factory() const { return _factory; }

  Factory &factory() { return _factory; }

  void parse(const std::string &str) {
    nlohmann::json data = nlohmann::json::parse(str);

    _factory.id = data["ID"].get<std::string>();
    parse_zones(data["zones"]);
    parse_items(data["items"]);
    parse_storages(data["storages"]);
    parse_catalog(data["catalog"]);
  }

private:
  std::shared_ptr<Buffer> parse_buffer(const nlohmann::json &data) {
    std::string ID = data["ID"].get<std::string>();
    Geometry geometry;
    uint capacity = data["capacity"].get<uint>();
    std::string capacity_unit = data.contains("capacity_unit") ? data["capacity_unit"].get<std::string>() : "PCE";
    std::string input_output_policy = data.contains("input_output_policy")
                                      ? data["input_output_policy"].get<std::string>() : "FIFO";
    int max_stay_duration = data.contains("max_stay_duration") ? data["max_stay_duration"].get<int>() : -1;
    bool is_batching = data.contains("is_batching") and data["is_batching"].get<bool>();
    uint batch_size = data.contains("batch_size") ? data["batch_size"].get<uint>() : 0;

    return std::make_shared<Buffer>(Buffer{ID, geometry, capacity,
                                           capacity_unit == "PCE" ? Buffer::PCE : capacity_unit == "L" ? Buffer::L :
                                                                                  capacity_unit == "KG" ? Buffer::KG
                                                                                                        : Buffer::M,
                                           input_output_policy == "FIFO" ? Buffer::FIFO : Buffer::LIFO,
                                           max_stay_duration, is_batching, batch_size});
  }

  void parse_buffers(const nlohmann::json &data, std::shared_ptr<Zone> &zone) {
    for (const nlohmann::json &item: data) {
      auto buffer = parse_buffer(item);

      zone->buffers[buffer->ID] = buffer;
      _factory.all_buffers[buffer->ID] = std::make_pair(_stock_index++, buffer);
    }
  }

  void parse_catalog(const nlohmann::json &data) {
    for (const nlohmann::json &product: data) {
      std::string product_id = product["ID"].get<std::string>();
      int batch_size = product.contains("batch_size") ? product["batch_size"].get<int>() : -1;
      std::shared_ptr<ProductCategory> category = product.contains("category") ? parse_category(product["category"])
                                                                               : nullptr;
      std::vector<Property> properties;
      Program program = parse_program(product["program"]);

      if (product.contains("properties")) {
        parse_properties(product["properties"]);
      }
      _factory.catalog[product_id] = std::make_pair(_product_index++,
                                                    std::make_unique<Product>(product_id, batch_size, category, program,
                                                                              properties));
    }
  }

  std::shared_ptr<ProductCategory> parse_category(const nlohmann::json &data) {
    // TODO
    return nullptr;
  }

  void parse_items(const nlohmann::json &data) {
    for (const nlohmann::json &item: data) {
      std::string ID = item["ID"].get<std::string>();
      std::string type = item.contains("type") ? item["type"].get<std::string>() : "raw";
      std::string quantity_unit = item.contains("quantity_unit") ? item["quantity_unit"].get<std::string>() : "PCE";

      _factory.items[ID] = std::make_shared<Item>(
        Item{ID, type == "raw" ? Item::RAW : type == "semi" ? Item::SEMI : Item::FINISHED,
             quantity_unit == "PCE" ? Item::PCE : quantity_unit == "L" ? Item::L : quantity_unit == "KG" ? Item::KG
                                                                                                         : Item::M,
             ItemGeometry{}});
      _factory.all_items[ID] = std::make_pair(_item_index++, _factory.items[ID]);
    }
  }

  void parse_labors(const nlohmann::json &data, std::shared_ptr<Zone> &zone) {
    // TODO
  }

  void parse_machines(const nlohmann::json &data, std::shared_ptr<Zone> &zone) {
    for (const nlohmann::json &m: data) {
      std::string ID = m["ID"].get<std::string>();
      std::string type = m["typeID"].get<std::string>();
      Geometry geometry;
      std::shared_ptr<MachineType> machine_type;
      uint loading_time = data.contains("loading_time") ? data["loading_time"].get<uint>() : 0;
      uint setup_time = data.contains("setup_time") ? data["setup_time"].get<uint>() : 0;
      uint processing_time = data.contains("processing_time") ? data["processing_time"].get<uint>() : 0;
      uint unloading_time = data.contains("unloading_time") ? data["unloading_time"].get<uint>() : 0;
      bool preemption = data.contains("preemption") && data["preemption"].get<bool>();
      uint batch_size = data.contains("batch_size") ? data["batch_size"].get<uint>() : -1;
      ResourceNeeds resource_needs;
      unsigned int capacity = data.contains("capacity") ? data["capacity"].get<int>() : 1;
      std::shared_ptr<Buffer> upstream_buffer = data.contains("upstream_bufferID") ? _factory.all_buffers.find(
        data["upstream_bufferID"])->second.second : nullptr;
      std::shared_ptr<Buffer> downstream_buffer = data.contains("downstream_bufferID") ? _factory.all_buffers.find(
        data["downstream_bufferID"])->second.second : nullptr;
      std::shared_ptr<Conveyor> upstream_conveyor; // TODO
      std::shared_ptr<Conveyor> downstream_conveyor; // TODO
      std::vector<std::shared_ptr<Labor>> labors; // TODO
      UnavailableCondition unavailable_condition; // TODO
      auto machine = std::shared_ptr<Machine>(new Machine{
        ID, type == "processor" ? Machine::PROCESSOR :
            (type == "robot" ? Machine::ROBOT :
             (type == "combiner" ? Machine::COMBINER :
              (type == "separator" ? Machine::SEPARATOR :
               (type == "multi_processor" ? Machine::MULTI_PROCESSOR : Machine::UNDEFINED)))),
        geometry, machine_type, setup_time, loading_time, processing_time, unloading_time,
        preemption, batch_size, resource_needs, capacity, upstream_buffer, downstream_buffer, upstream_conveyor,
        downstream_conveyor, labors, unavailable_condition});

      zone->machines[ID] = machine;
      _factory.all_machines[ID] = std::make_pair(_machine_index++, machine);
    }
  }

  std::shared_ptr<Operation> parse_operation(const nlohmann::json &data) {
    uint loading_time = data.contains("loading_time") ? data["loading_time"].get<uint>() : 0;
    uint setup_time = data.contains("setup_time") ? data["setup_time"].get<uint>() : 0;
    uint processing_time = data.contains("processing_time") ? data["processing_time"].get<uint>() : 0;
    uint unloading_time = data.contains("unloading_time") ? data["unloading_time"].get<uint>() : 0;
    std::string machine_type_ID = data.contains("machine_type_ID") ? data["machine_type_ID"].get<std::string>() : "";
    std::shared_ptr<Zone> zone_ID = _factory.all_zones.find(data["zoneID"].get<std::string>())->second.zone;
    std::shared_ptr<Machine> machine_ID = _factory.all_machines.find(
      data["machineID"].get<std::string>())->second.second;
    std::vector<Part> ins = data.contains("ins") ? parse_parts(data["ins"]) : std::vector<Part>();
    std::vector<Part> outs = data.contains("outs") ? parse_parts(data["outs"]) : std::vector<Part>();

    if (data.contains("next_operation")) {
      auto next_operation = parse_operation(data["next_operation"]);

      return std::shared_ptr<Operation>(
        new Operation{loading_time, setup_time, processing_time, unloading_time, next_operation, nullptr,
                      machine_type_ID, zone_ID,
                      machine_ID, ins, outs});
    } else if (data.contains("next_operator")) {
      auto next_operator = parse_operator(data["next_operator"]);

      return std::shared_ptr<Operation>(
        new Operation{loading_time, setup_time, processing_time, unloading_time, nullptr, next_operator,
                      machine_type_ID, zone_ID,
                      machine_ID, ins, outs});
    }
    return std::shared_ptr<Operation>(
      new Operation{loading_time, setup_time, processing_time, unloading_time, nullptr, nullptr,
                    machine_type_ID, zone_ID,
                    machine_ID, ins, outs});
  }

  std::shared_ptr<Operator> parse_operator(const nlohmann::json &data) {
    //TODO
    return nullptr;
  }

  std::vector<Part> parse_parts(const nlohmann::json &data) {
    std::vector<Part> parts;

    for (const nlohmann::json &part: data) {
      std::string item_id = part["itemID"].get<std::string>();
      double quantity = part["quantity"].get<double>();
      auto item = _factory.items.find(item_id)->second;
      std::string storage_id = part.contains("location") ? (part["location"].contains("storageID") ? part["location"]["storageID"].get<std::string>() : "") : "";
      std::string buffer_id = part.contains("location") ? (part["location"].contains("bufferID") ? part["location"]["bufferID"].get<std::string>() : "") : "";

      assert(not storage_id.empty() or not buffer_id.empty());
      assert(storage_id.empty() or _factory.all_storages.find(storage_id) != _factory.all_storages.end());
      assert(buffer_id.empty() or _factory.all_buffers.find(buffer_id) != _factory.all_buffers.end());

      parts.push_back(Part{item, storage_id.empty() ? nullptr : _factory.all_storages.find(storage_id)->second.second,
                           buffer_id.empty() ? nullptr : _factory.all_buffers.find(buffer_id)->second.second,
                           quantity});
    }
    return parts;
  }

  Program parse_program(const nlohmann::json &data) {
    if (data.contains("first_operation")) {
      auto op = parse_operation(data["first_operation"]);

      return {op};
    } else if (data.contains("first_operator")) {
      auto op = parse_operator(data["first_operator"]);

      return {op};
    } else {
      assert(false);
    }
    return {};
  }

  std::vector<Property> parse_properties(const nlohmann::json &data) {
    // TODO
    return {};
  }

  void parse_sub_zones(const nlohmann::json &data, std::shared_ptr<Zone> &zone, std::vector<std::string> path) {
    for (const nlohmann::json &z: data) {
      std::string zone_id = z["ID"].get<std::string>();
      auto sub_zone = parse_zone(z, path);

      zone->sub_zones[zone_id] = sub_zone;
      _factory.all_zones[zone_id] = {_zone_index++, sub_zone, path};
    }
  }

  std::shared_ptr<Storage> parse_storage(const nlohmann::json &data) {
    std::shared_ptr<Storage> storage;

    std::string ID = data["ID"].get<std::string>();
    Geometry geometry;
    int capacity = data["capacity"].get<int>();
    std::string capacity_unit = data.contains("capacity_unit") ? data["capacity_unit"].get<std::string>() : "PCE";
    std::shared_ptr<Slots> slots; // TODO
    ResourceNeeds resource_needs; // TODO
    std::vector<Labor> labors; // TODO
    std::vector<Part> initial; // TODO

    if (data.contains("initial")) {
      for (const nlohmann::json &initial_json: data["initial"]) {
        std::string itemID = initial_json["itemID"].get<std::string>();
        double quantity = initial_json["quantity"].get<double>();
        auto item = _factory.items.find(itemID)->second;

        initial.push_back(Part{item, nullptr, nullptr, quantity});
      }
    }
    return std::make_shared<Storage>(Storage{ID, geometry, capacity,
                                             capacity_unit == "PCE" ? Storage::PCE : capacity_unit == "L" ? Storage::L :
                                                                                     capacity_unit == "KG" ? Storage::KG
                                                                                                           : Storage::M,
                                             slots, resource_needs, labors, initial});
  }

  void parse_storages(const nlohmann::json &data, std::shared_ptr<Zone> &zone) {
    for (const nlohmann::json &s: data) {
      std::shared_ptr<Storage> storage = parse_storage(s);

      zone->storages[storage->ID] = storage;
      _factory.all_storages[storage->ID] = std::make_pair(_stock_index++, storage);
    }
  }

  void parse_storages(const nlohmann::json &data) {
    for (const nlohmann::json &s: data) {
      std::shared_ptr<Storage> storage = parse_storage(s);

      _factory.storages[storage->ID] = storage;
      _factory.all_storages[storage->ID] = std::make_pair(_stock_index++, storage);
    }
  }

  std::shared_ptr<Zone> parse_zone(const nlohmann::json &zone_data, std::vector<std::string> path) {
    std::string ID = zone_data["ID"].get<std::string>();
    std::string type_ID = zone_data["typeID"].get<std::string>();
    std::shared_ptr<ZoneType> type;

    if (std::find_if(_factory.zone_types.cbegin(), _factory.zone_types.cend(),
                     [type_ID](const auto &e) { return e->id == type_ID; }) == _factory.zone_types.cend()) {
      type = std::make_shared<ZoneType>(ZoneType{type_ID});
      _factory.zone_types.push_back(type);
    }

    Geometry geometry;
    std::shared_ptr<Zone> zone(new Zone{ID, geometry, type});

    if (zone_data.contains("sub_zones")) {
      path.push_back(ID);
      parse_sub_zones(zone_data["sub_zones"], zone, path);
    }
    if (zone_data.contains("storages")) {
      parse_storages(zone_data["storages"], zone);
    }
    if (zone_data.contains("buffers")) {
      parse_buffers(zone_data["buffers"], zone);
    }
    if (zone_data.contains("machines")) {
      parse_machines(zone_data["machines"], zone);
    }
    if (zone_data.contains("labors")) {
      parse_labors(zone_data["labors"], zone);
    }
    return zone;
  }

  void parse_zones(const nlohmann::json &data) {
    for (const nlohmann::json &z: data) {
      std::string zone_id = z["ID"].get<std::string>();
      auto zone = parse_zone(z, {});

      _factory.zones[zone_id] = zone;
      _factory.all_zones[zone_id] = {_zone_index++, zone, {}};
    }
  }

  Factory _factory;
  uint _item_index = 0;
  uint _machine_index = 0;
  uint _product_index = 0;
  uint _stock_index = 0; // storage and buffer
  uint _zone_index = 0;
};

}

#endif //ARTIS_FACTORY_JSON_DSL_JSON_READER_HPP
