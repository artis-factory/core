/**
 * @file json_dsl/model/Model.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_FACTORY_JSON_DSL_MODEL_HPP
#define ARTIS_FACTORY_JSON_DSL_MODEL_HPP

#include <map>
#include <stack>
#include <string>
#include <utility>
#include <vector>

namespace artis::factory::json_dsl {

struct Coordinates {
};

struct Cell2D : Coordinates {
  int x;
  int y;
};

struct Point2D : Coordinates {
  int x;
  int y;
};

struct Geometry {
  enum {
    CELLULAR, POLYGON
  } type;
  std::vector<Cell2D> cells;
  std::vector<Point2D> points;
};

struct Buffer {
  std::string ID;
  Geometry geometry;
  uint capacity;
  enum {
    PCE, L, KG, M
  } capacity_unit;
  enum {
    FIFO, LIFO
  } input_output_policy;
  int max_stay_duration;
  bool is_batching;
  uint batch_size;
};

struct Distribution {
  enum {
    INTEGER, REAL
  } type;
};

struct UniformDistribution : Distribution {
  double min;
  double max;
  double step;
};

struct NormalDistribution : Distribution {
  double mean;
  double standard_deviation;
};

struct ExponentialDistribution : Distribution {
  double min;
  double rate;
};

struct TriangularDistribution : Distribution {
  double min;
  double max;
  double mode;
};

struct UnavailableCondition {
  enum {
    BREAKDOWN, MAINTENANCE, PROGRAM_CHANGE
  } type;
  int down_duration;
  Distribution down_duration_distribution;
  int before_first_duration;
  Distribution before_first_duration_distribution;
  int before_first_operation_number;
  Distribution before_first_operation_number_distribution;
  int up_operation_number;
  Distribution up_operation_number_distribution;
};

struct Transportation {
  std::string id;
  int speed;
  UnavailableCondition unavailable_condition;
};

struct Path {
  std::vector<std::shared_ptr<Coordinates>> coordinates;
};

struct AGV : Transportation {
  std::shared_ptr<Coordinates> initial_location;
  std::vector<Path> paths;
};

struct Machine;

struct Point3D {
  int x;
  int y;
  int z;
};

struct ConveyorElement {
  Point3D start;
  Point3D end;
  double length;
  double width;
  int capacity;
  enum {
    PCE, L, KG, M
  } capacity_unit;
  double angle;
};

struct Conveyor : Transportation {
  int length;
  int capacity;
  enum {
    PCE, L, KG, M
  } capacity_unit;
  int item_space;
  std::vector<ConveyorElement> elements;
  std::shared_ptr<Machine> source;
  std::shared_ptr<Machine> destination;
};

struct ItemGeometry {
  double length{};
  double width{};
  double height{};
};

struct Item {
  std::string id;
  enum {
    RAW, SEMI, FINISHED
  } type;
  enum {
    PCE, L, KG, M
  } quantity_unit;
  ItemGeometry geometry;
};

struct ItemArrival {
  std::shared_ptr<Item> item;
  int quantity;
  std::vector<int> quantities;
  std::shared_ptr<Distribution> quantity_distribution;
  int delay;
  std::vector<int> delays;
  std::shared_ptr<Distribution> delay_distribution;
  std::vector<int> dates;

  ItemArrival() {}
};

struct Skill;

struct Labor {
  std::string id;
  std::shared_ptr<Skill> skill;
  UnavailableCondition unavailable_condition;
};

struct MachineType {
  std::string id;
};

struct ResourceNeeds {
  int labor_number;
  std::vector<std::pair<int, std::vector<std::string>>> labor_skills;
  int transporter_number;
  std::vector<std::pair<int, std::string> > transporter_types;
};

struct Machine {
  std::string ID;
  enum MachineSubType {
    UNDEFINED, PROCESSOR, ROBOT, COMBINER, SEPARATOR, MULTI_PROCESSOR
  } type;
  Geometry geometry;
  std::shared_ptr<MachineType> machine_type;
  uint setup_time;
  uint loading_time;
  uint processing_time;
  uint unloading_time;
  bool preemption;
  uint batch_size;
  ResourceNeeds resource_needs;
  uint capacity;
  std::shared_ptr<Buffer> upstream_buffer;
  std::shared_ptr<Buffer> downstream_buffer;
  std::shared_ptr<Conveyor> upstream_conveyor;
  std::shared_ptr<Conveyor> downstream_conveyor;
  std::vector<std::shared_ptr<Labor>> labors;
  UnavailableCondition unavailable_condition;
};

struct Operator;
struct Storage;

struct Part {
  std::shared_ptr<Item> item;
  std::shared_ptr<Storage> storage;
  std::shared_ptr<Buffer> buffer;
  double quantity;
};

struct Zone;

struct Operation {
  uint loading_time;
  uint setup_time;
  uint processing_time;
  uint unloading_time;
  std::shared_ptr<Operation> next_operation{nullptr};
  std::shared_ptr<Operator> next_operator{nullptr};
  std::string machine_type_ID{};
  std::shared_ptr<Zone> zone_ID{nullptr};
  std::shared_ptr<Machine> machine_ID{nullptr};
  std::vector<Part> in{};
  std::vector<Part> out{};
};

struct VariantValue;

struct Operator {
  enum {
    VARIANT, ALTERNATIVE, PARALLEL
  } type;
  std::vector<std::shared_ptr<Operation>> operations;
  std::vector<VariantValue> variant_values;
};

struct ProductCategory {
  std::string id;
};

struct Property {
  std::string id;
  std::string unit;

};

struct Program {
  std::shared_ptr<Operation> first_operation;
  std::shared_ptr<Operator> first_operator;

  Program() : first_operation(nullptr), first_operator(nullptr) {}

  Program(std::shared_ptr<Operation> &op) : first_operation(op), first_operator(nullptr) {}

  Program(std::shared_ptr<Operator> &op) : first_operation(nullptr), first_operator(op) {}
};

struct Product {
  std::string id;
  int batch_size;
  std::shared_ptr<ProductCategory> category_ID;
  Program program;
  std::vector<Property> properties;

  Product(std::string &id, int batch_size, std::shared_ptr<ProductCategory> &category_ID,
          Program &program, std::vector<Property> &properties) : id(id), batch_size(batch_size),
                                                                 category_ID(category_ID),
                                                                 program(program),
                                                                 properties(properties) {}
};

struct OrderVariant {
  std::shared_ptr<Property> property;
  std::string value;
};

struct OrderArrival {
  std::shared_ptr<Product> product;
  std::vector<OrderVariant> variants;
  int quantity;
  std::vector<int> quantities;
  std::shared_ptr<Distribution> quantity_distribution;
  int delay;
  std::vector<int> delays;
  std::shared_ptr<Distribution> delay_distribution;
  std::vector<int> dates;
};

struct Skill {
  std::string id;
};

enum Order {
  Xp_Yp,
  Xp_Ym,
  Xm_Yp,
  Xm_Ym,
  Yp_Xp,
  Yp_Xm,
  Ym_Xp,
  Ym_Xm,
  Xp_Zp,
  Xp_Zm,
  Xm_Zp,
  Xm_Zm,
  Zp_Xp,
  Zp_Xm,
  Zm_Xp,
  Zm_Xm,
  Xp_Yp_Zp,
  Xp_Yp_Zm,
  Xp_Ym_Zp,
  Xp_Ym_Zm,
  Xm_Yp_Zp,
  Xm_Yp_Zm,
  Xm_Ym_Zp,
  Xm_Ym_Zm,
  Xp_Zp_Yp,
  Xp_Zp_Ym,
  Xp_Zm_Yp,
  Xp_Zm_Ym,
  Xm_Zp_Yp,
  Xm_Zp_Ym,
  Xm_Zm_Yp,
  Xm_Zm_Ym,
  Yp_Xp_Zp,
  Yp_Xp_Zm,
  Yp_Xm_Zp,
  Yp_Xm_Zm,
  Ym_Xp_Zp,
  Ym_Xp_Zm,
  Ym_Xm_Zp,
  Ym_Xm_Zm,
  Yp_Zp_Xp,
  Yp_Zp_Xm,
  Yp_Zm_Xp,
  Yp_Zm_Xm,
  Ym_Zp_Xp,
  Ym_Zp_Xm,
  Ym_Zm_Xp,
  Ym_Zm_Xm,
  Zp_Yp_Xp,
  Zp_Yp_Xm,
  Zp_Ym_Xp,
  Zp_Ym_Xm,
  Zm_Yp_Xp,
  Zm_Yp_Xm,
  Zm_Ym_Xp,
  Zm_Ym_Xm,
  Zp_Xp_Yp,
  Zp_Xp_Ym,
  Zp_Xm_Yp,
  Zp_Xm_Ym,
  Zm_Xp_Yp,
  Zm_Xp_Ym,
  Zm_Xm_Yp,
  Zm_Xm_Ym
};

struct Slots {
  enum {
    XY, XZ, XYZ
  } type;
  Order stocking_in_slot_order;
  enum {
    FIRST
  } stocking_strategy;
  Order taking_in_slot_order;
  enum {
    FIRST_FIND
  } taking_strategy;
  int x_number;
  int x_length;
  int y_number;
  int y_length;
  int z_number;
  int z_length;
};

struct Storage {
  std::string ID;
  Geometry geometry;
  int capacity;
  enum {
    PCE, L, KG, M
  } capacity_unit;
  std::shared_ptr<Slots> slots;
  ResourceNeeds resource_needs;
  std::vector<Labor> labors;
  std::vector<Part> initial;
};

struct TransporterType {
  std::string id;
};

struct Transporter : Transportation {
  std::shared_ptr<Coordinates> initial_location;
  std::vector<Path> paths;
  ResourceNeeds resource_needs;
  std::vector<std::shared_ptr<Labor>> labors;
  std::shared_ptr<TransporterType> type;
};

struct VariantValue {
  std::shared_ptr<Property> property;
  std::string value;
};

struct ZoneType {
  std::string id;
};

struct Zone {
  std::string ID;
  Geometry geometry;
  std::shared_ptr<ZoneType> type;
  std::map<std::string, std::shared_ptr<Zone>> sub_zones;
  std::map<std::string, std::shared_ptr<Storage>> storages;
  std::map<std::string, std::shared_ptr<Buffer>> buffers;
  std::map<std::string, std::shared_ptr<Machine>> machines;
  std::map<std::string, std::shared_ptr<Labor>> labors;
};

struct Factory {
  std::string id;
  std::map<std::string, std::pair<uint, std::shared_ptr<Product>>> catalog;
  std::vector<ItemArrival> item_arrivals;
  std::map<std::string, std::shared_ptr<Item>> items;
  std::map<std::string, std::shared_ptr<Labor>> labors;
  std::vector<OrderArrival> order_arrivals;
  std::map<std::string, std::shared_ptr<Storage>> storages;
  std::map<std::string, std::shared_ptr<Transportation>> transportations;
  std::map<std::string, std::shared_ptr<Zone>> zones;

  std::vector<std::shared_ptr<MachineType>> machine_types;
  std::vector<std::shared_ptr<TransporterType>> transporter_types;
  std::vector<std::shared_ptr<ZoneType>> zone_types;
  std::vector<std::shared_ptr<ProductCategory>> product_categories;
  std::vector<std::shared_ptr<Skill>> skills;
  std::vector<std::shared_ptr<Property>> properties;

  struct AllZone {
    uint index;
    std::shared_ptr<Zone> zone;
    std::vector<std::string> path;
  };

  std::map<std::string, std::pair<uint, std::shared_ptr<Buffer>>> all_buffers;
  std::map<std::string, std::pair<uint, std::shared_ptr<Item>>> all_items;
  std::map<std::string, std::pair<uint, std::shared_ptr<Machine>>> all_machines;
  std::map<std::string, std::pair<uint, std::shared_ptr<Storage>>> all_storages;
  std::map<std::string, AllZone> all_zones;

  Factory() {}

//  struct FactoryStorages {
//    using Machines = std::pair<std::vector<uint>, std::vector<uint>>; // zone, machine
//
//    std::map<uint, Machines> in_machines; // storage -> machine (in)
//    std::map<uint, Machines> out_machines; // storage -> machine (out)
//  };



//  FactoryStorages get_factory_storages() const {
//    FactoryStorages storages;
//
//    for (const auto &product: catalog) {
//      auto op = product.second.second->program.first_operation;
//
//      if (op) {
//        while (op) {
//          uint machine_id = all_machines.find(op->machine_ID->ID)->second.first;
//          uint zone_id = all_zones.find(zone_of_machine(op->machine_ID->ID)->ID)->second.index;
//
//          for (const auto &part: op->out) {
//            if (part.storage) {
//              uint storageID = all_storages.find(part.storage->ID)->second.first;
//
//              if (storages.in_machines.find(storageID) == storages.in_machines.end()) {
//                storages.in_machines[storageID] = FactoryStorages::Machines();
//              }
//              if (std::find(storages.in_machines[storageID].second.begin(), storages.in_machines[storageID].second.end(),
//                             machine_id) == storages.in_machines[storageID].second.end()) {
//                storages.in_machines[storageID].first.push_back(zone_id);
//                storages.in_machines[storageID].second.push_back(machine_id);
//              }
//            }
//          }
//          for (const auto &part: op->in) {
//            if (part.storage) {
//              uint storageID = all_storages.find(part.storage->ID)->second.first;
//
//              if (storages.out_machines.find(storageID) == storages.out_machines.end()) {
//                storages.out_machines[storageID] = FactoryStorages::Machines();
//              }
//              if (std::find(storages.out_machines[storageID].second.begin(), storages.out_machines[storageID].second.end(),
//                            machine_id) == storages.out_machines[storageID].second.end()) {
//                storages.out_machines[storageID].first.push_back(zone_id);
//                storages.out_machines[storageID].second.push_back(machine_id);
//              }
//            }
//          }
//          op = op->next_operation;
//        }
//      }
//    }
//    return storages;
//  }


};

}

#endif //ARTIS_FACTORY_JSON_DSL_MODEL_HPP
