/**
 * @file Sink.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_FACTORY_SINK_HPP
#define ARTIS_FACTORY_SINK_HPP

#include "Base.hpp"
#include "Machine.hpp"
#include "ProductionOrder.hpp"

namespace artis::factory {

struct SinkParameters : MachineParameters {
};

class Sink : public Dynamics<Sink, SinkParameters> {
public:
  struct inputs {
    enum values {
      IN
    };
  };

  struct outputs {
    enum values {
      OUT
    };
  };

  struct vars {
    enum values {
      FINISHED_PO_NUMBER, MIN_LATENESS, MAX_LATENESS, TOTAL_LATENESS, LATE_JOB_NUMBER
    };
  };

  Sink(const std::string &name, const Context<Sink, SinkParameters> &context)
    : Dynamics<Sink, SinkParameters>(name, context), _parameters(context.parameters()) {
    input_port({inputs::IN, "in"});
    output_port({outputs::OUT, "out"});
    observables({{vars::FINISHED_PO_NUMBER, "finished_po_number"},
                 {vars::MIN_LATENESS, "min_lateness"},
                 {vars::MAX_LATENESS, "max_lateness"},
                 {vars::TOTAL_LATENESS, "total_lateness"},
                 {vars::LATE_JOB_NUMBER, "late_job_number"}});
  }

  ~Sink() override = default;

  void dint(const Time &t) override;

  void dext(const Time &t, const Time &e, const Bag &bag) override;

  void start(const Time &t) override;

  Time ta(const Time &t) const override;

  Bag lambda(const Time &t) const override;

  artis::common::event::Value observe(const Time &t, unsigned int index) const override;

private:
  struct Phase {
    enum values {
      WAIT, SEND
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case WAIT:
          return "WAIT";
        case SEND:
          return "SEND";
      }
      return "";
    }
  };

  typedef std::deque<std::pair<double, std::unique_ptr<ProductionOrder>>> ProductionOrders;

  SinkParameters _parameters;

  Phase::values _phase;
  ProductionOrders _finished_po;
};

} // namespace artis::factory

#endif