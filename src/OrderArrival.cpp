/**
 * @file OrderArrival.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "OrderArrival.hpp"

namespace artis::factory {

void OrderArrival::dint(const Time &t) {
  switch (_phase) {
    case Phase::INIT:
      _phase = Phase::SEND;
      break;
    case Phase::SEND: {
      if (_parameters.po_number == -1) {
        _pos.clear();
        generate_production_orders(t);
        _sigma = _distrib_send_speed(_rng);
        _phase = Phase::SEND;
      } else {
        if (_po_index < (unsigned int) _parameters.po_number - 1) {
          _po_index++;
          if (not _parameters.po_order.empty()) {
            _sigma = 0;
          }
        } else {
          _phase = Phase::FINISH;
        }
      }
      break;
    }
    case Phase::FINISH:
      assert(false);
  }
}

void OrderArrival::dext(const Time & /* t */, const Time & /* e */, const Bag &bag) {
  std::for_each(bag.begin(), bag.end(), [](const ExternalEvent & /* event */) {
    assert(false);
  });
}

void OrderArrival::start(const Time &t) {
  _phase = Phase::INIT;
  _current_id = 0;
  _po_index = 0;
  if (_parameters.po_number == -1) {
    generate_production_orders(t);
  } else {
    for (unsigned int i = 0; i < (unsigned int) _parameters.po_number; ++i) {
      generate_production_orders(t);
    }
  }
  _sigma = 0;
}

Time OrderArrival::ta(const Time & /* t */) const {
  switch (_phase) {
    case Phase::INIT:
      return 0;
    case Phase::SEND:
      return _sigma;
    case Phase::FINISH:
      return artis::common::DoubleTime::infinity;
  }
  return artis::common::DoubleTime::infinity;
}

Bag OrderArrival::lambda(const Time & /* t */) const {
  Bag bag;

  if (_phase == Phase::SEND) {
    if (_parameters.po_order.empty()) {
      bag.push_back(
        ExternalEvent(outputs::OUT, common::event::Value(_pos[_po_index]->_buffer, _pos[_po_index]->_size)));
    } else {
      const auto &po = _pos[_parameters.po_order[_po_index]];

      bag.push_back(ExternalEvent(outputs::OUT, common::event::Value(po->_buffer, po->_size)));
    }
  }
  return bag;
}

artis::common::event::Value OrderArrival::observe(const Time & /* t */, unsigned int index) const {
  switch (index) {
    case vars::TOTAL_PO_NUMBER:
      return (unsigned int) _po_index;
  }
  return {};
}

void OrderArrival::generate_production_orders(const Time &t) {
  std::discrete_distribution<> dis(_parameters.product_proportions.begin(), _parameters.product_proportions.end());

  _productID = _productIDs[dis(_rng)];

  std::uniform_int_distribution<> _distrib_due_date(_parameters.product_processing_durations[_productID],
                                                    _parameters.product_processing_durations[_productID] +
                                                    _parameters.due_date_window);
  unsigned int due_date = t + _distrib_due_date(_rng);

  _pos.push_back(std::make_unique<ProductionOrder>(_current_id, _current_id, due_date, _productID,
                                                   _parameters.programs[_productID]));
  _current_id++;
}

} // namespace artis::factory
