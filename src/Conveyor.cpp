/**
 * @file Conveyor.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Conveyor.hpp"

namespace artis::factory {

void Conveyor::dint(const Time & /* t */) {
  // TODO
}

void Conveyor::dext(const Time & /* t */, const Time & /* e */, const Bag &bag) {
  std::for_each(bag.begin(), bag.end(), [](const ExternalEvent & /* event */) {
    // TODO
  });
}

void Conveyor::start(const Time & /* t */) {
  // TODO
}

Time Conveyor::ta(const Time & /* t */) const {
  // TODO
  return artis::common::DoubleTime::infinity;
}

Bag Conveyor::lambda(const Time & /* t */) const {
  Bag bag;

  // TODO
  return bag;
}

artis::common::event::Value Conveyor::observe(const Time & /* t */, unsigned int /* index */) const {
  return {};
}

} // namespace artis::factory