/**
 * @file ZoneRouter.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_FACTORY_POOL_ROUTER_HPP
#define ARTIS_FACTORY_POOL_ROUTER_HPP

#include "Base.hpp"
#include "dsl_to_devs/Model.hpp"
#include "ProductionOrder.hpp"

#include <deque>

namespace artis::factory {

struct ZoneRouterParameters {
  std::string ID;
  uint index;
  dsl_to_devs::Machines machines;
  dsl_to_devs::Zones sub_zones;
};

class ZoneRouter : public Dynamics<ZoneRouter, ZoneRouterParameters> {
public:
  struct inputs {
    enum values {
      IN_PO, FINISH, IN_M = 1000, IN_SZ = 2000
    };
  };

  struct outputs {
    enum values {
      OUT_PO, OUT_M = 1000, OUT_SZ = 2000
    };
  };

  struct vars {
    enum values {
      WAITING_PO_NUMBER
    };
  };

  ZoneRouter(const std::string &name, const Context<ZoneRouter, ZoneRouterParameters> &context)
    : Dynamics<ZoneRouter, ZoneRouterParameters>(name, context), _parameters(context.parameters()) {
    input_ports({{inputs::IN_PO,  "in_po"},
                 {inputs::FINISH, "finish"}});
    output_port({outputs::OUT_PO, "out_po"});
    for (const auto &machine: context.parameters().machines) {
      output_port({outputs::OUT_M + machine.index, "out_m_" + std::to_string(machine.index)});
      input_port({inputs::IN_M + machine.index, "in_m_" + std::to_string(machine.index)});
    }
    for (const auto &z: context.parameters().sub_zones) {
      output_port({outputs::OUT_SZ + z.first, "out_sz_" + std::to_string(z.first)});
      input_port({inputs::IN_SZ + z.first, "in_sz_" + std::to_string(z.first)});
    }
    observables({{vars::WAITING_PO_NUMBER, "waiting_po_number"}});
  }

  ~ZoneRouter() override = default;

  void dint(const Time & /* t */) override;

  void dext(const Time & /* t */, const Time & /* e */, const Bag & /* bag*/) override;

  void start(const Time & /* t */) override;

  Time ta(const Time & /* t */) const override;

  Bag lambda(const Time & /* t */) const override;

  artis::common::event::Value observe(const Time &t, unsigned int index) const override;

private:
  using ProductionOrders = std::deque<std::unique_ptr<ProductionOrder>>;

  ProductionOrders::const_iterator next_po() const;

  struct Phase {
    enum values {
      INIT, WAIT, SEND
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case INIT:
          return "INIT";
        case WAIT:
          return "WAIT";
        case SEND:
          return "SEND";
      }
      return "";
    }
  };

  // parameters
  ZoneRouterParameters _parameters;

  // state
  Phase::values _phase;
  ProductionOrders _pending_po;
  std::map<unsigned int, unsigned int> _available_machines;
};

} // namespace artis::factory

#endif