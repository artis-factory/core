/**
 * @file Base.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_FACTORY_BASE_HPP
#define ARTIS_FACTORY_BASE_HPP

#include <artis-star/common/event/Bag.hpp>
#include <artis-star/common/event/ExternalEvent.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>
#include <artis-star/common/observer/View.hpp>
#include <artis-star/common/utils/Trace.hpp>
#include <artis-star/common/time/DoubleTime.hpp>
#include <artis-star/kernel/pdevs/Dynamics.hpp>

namespace artis::factory {

typedef typename artis::common::DoubleTime::type Time;
typedef typename artis::common::event::ExternalEvent<artis::common::DoubleTime> ExternalEvent;
typedef typename artis::common::event::Bag<artis::common::DoubleTime> Bag;
typedef typename artis::common::observer::View<artis::common::DoubleTime> View;
typedef typename artis::common::observer::Output<artis::common::DoubleTime, artis::common::observer::TimedIterator<artis::common::DoubleTime>> Output;
typedef typename artis::common::Coordinator<artis::common::DoubleTime> Coordinator;
typedef typename artis::common::Trace<artis::common::DoubleTime> Trace;
typedef typename artis::common::TraceElement<artis::common::DoubleTime> TraceElement;

template<class Model, class Parameters>
using Context = artis::pdevs::Context<artis::common::DoubleTime, Model, Parameters>;

template<class Model, class Parameters>
class Dynamics : public artis::pdevs::Dynamics<artis::common::DoubleTime, Model, Parameters> {
public:
  Dynamics(const std::string &name, const Context<Model, Parameters> &context)
    : artis::pdevs::Dynamics<artis::common::DoubleTime, Model, Parameters>(name, context) {}

  ~Dynamics() override = default;

  void dconf(const Time &t, const Time &e, const Bag &bag) override {
    this->dint(t);
    this->dext(t, e, bag);
  }
};

} // namespace artis::factory

#endif