/**
 * @file Sink.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Sink.hpp"

#include <numeric>

namespace artis::factory {

void Sink::dint(const Time & /* t */) {
  switch (_phase) {
    case Phase::WAIT: {
      assert(false);
    }
    case Phase::SEND: {
      _phase = Phase::WAIT;
    }
  }
}

void Sink::dext(const Time &t, const Time & /* e */, const Bag &bag) {
  std::for_each(bag.begin(), bag.end(), [t, this](const ExternalEvent &event) {
    if (event.port_index() == inputs::IN) {
      uint8_t *data = nullptr;

      event.data()(data);
      _finished_po.emplace_back(t, std::make_unique<ProductionOrder>(data, event.data().size()));
      _phase = Phase::SEND;

#ifdef WITH_TRACE
      Trace::trace()
        << TraceElement(get_name(), t,
                        artis::common::FormalismType::PDEVS,
                        artis::common::FunctionType::DELTA_EXT,
                        artis::common::LevelType::USER)
        << "FINISH po = " << _finished_po.back().second->to_string();
      Trace::trace().flush();
#endif
    }
  });
}

void Sink::start(const Time & /* t */) {
  _phase = Phase::WAIT;
}

Time Sink::ta(const Time & /* t */) const {
  switch (_phase) {
    case Phase::WAIT: {
      return artis::common::DoubleTime::infinity;
    }
    case Phase::SEND: {
      return 0;
    }
  }
  return artis::common::DoubleTime::infinity;
}

Bag Sink::lambda(const Time & /* t */) const {
  Bag bag;

  if (_phase == Phase::SEND) {
    bag.push_back(ExternalEvent(outputs::OUT, (unsigned int) _parameters.machine_id));
  }
  return bag;
}

artis::common::event::Value Sink::observe(const Time & /* t */, unsigned int index) const {
  switch (index) {
    case vars::FINISHED_PO_NUMBER:
      return (unsigned int) _finished_po.size();
    case vars::MIN_LATENESS: {
      uint min = 1e9;

      for (const auto &e: _finished_po) {
        if (e.first > e.second->get_due_date() and min > (uint) e.first - e.second->get_due_date()) {
          min = (uint) e.first - e.second->get_due_date();
        }
      }
      if (min == 1e9) {
        return (unsigned int) 0;
      } else {
        return (unsigned int) min;
      }
    }
    case vars::MAX_LATENESS: {
      uint max = 0;

      for (const auto &e: _finished_po) {
        if (e.first > e.second->get_due_date() and max < (uint) e.first - e.second->get_due_date()) {
          max = (uint) e.first - e.second->get_due_date();
        }
      }
      return (unsigned int) max;
    }
    case vars::TOTAL_LATENESS: {
      uint sum = 0;

      for (const auto &e: _finished_po) {
        sum += e.first > e.second->get_due_date() ? (uint) e.first - e.second->get_due_date() : 0;
      }
      return (unsigned int) sum;
    }
    case vars::LATE_JOB_NUMBER: {
      uint sum = 0;

      for (const auto &e: _finished_po) {
        if (e.first > e.second->get_due_date()) {
          ++sum;
        }
      }
      return (unsigned int) sum;
    }
  }
  return {};
}

} // namespace artis::factory